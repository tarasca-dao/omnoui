sdk=../Android/Sdk
btver=31.0.0
buildtools=$sdk/build-tools/$btver

mkdir $sdk -p

for f in ../*.zip
do
unzip -nd $sdk $f 
break
done

$sdk/cmdline-tools/bin/sdkmanager --sdk_root=$sdk --install "build-tools;$btver"

ln -sf d8 $buildtools/dx
ln -sf d8.jar $buildtools/lib/dx.jar

echo "sdk.dir=$sdk" > "local.properties"

./gradlew android:packageDebug

ls -l android/build/outputs/apk/debug/*.apk
