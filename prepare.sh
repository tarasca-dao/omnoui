dbuild=build

git clone https://github.com/Tarasca-DAO/mythicalbeings-media.git media

git clone https://bitbucket.org/tarasca-dao/uilibgdx/src/master/ uigdx
git clone https://bitbucket.org/tarasca-dao/tarascadaoomno/src/master/ omno
git clone https://bitbucket.org/tarasca-dao/omnoui/src/master/ omnoui

mkdir $dbuild

cp -R uigdx/* $dbuild/
cp -R omno/* $dbuild/core/
cp -R omnoui/* $dbuild/
cp -R media/images/* $dbuild/android/assets/texture/included/

wget -c https://storage.googleapis.com/google-code-archive-source/v2/code.google.com/json-simple/source-archive.zip
unzip -d ./ source-archive.zip
cp -R json-simple/trunk/src/main/java/org $dbuild/core/src/

cd build
sh android/build.sh
./gradlew desktop:dist
