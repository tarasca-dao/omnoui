package concept.ui;

import concept.omno.ApplicationContext;
import concept.omno.RemoteApi;
import concept.platform.ArdorApi;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Battle;

import java.util.ArrayList;
import java.util.List;

public class BattleLog {

    final ArdorApi ardorApi;
    final concept.omno.ApplicationContext omno;
    final RemoteApi remoteApi;

    final List<Battle> battleList = new ArrayList<>();

    public BattleLog(ApplicationContext omno, ArdorApi ardorApi) {
        this.ardorApi = ardorApi;
        this.omno = omno;
        this.remoteApi = omno.remoteApi;
    }

    public boolean update() {

        boolean result = false;

        synchronized (omno) {

            while (battleList.size() != 0) {

                int index = battleList.size() - 1;

                Battle battle = battleList.get(index);

                if (battle == null || !battle.economicCluster.isValid(ardorApi)) {
                    battleList.remove(index);
                    result = true;
                    continue;
                }

                break;
            }

            if (omno.state.rgame.arena.battleCount == 0) {
                return result;
            }

            int indexLast = battleList.size() - 1;

            Battle battleLast = null;

            if (indexLast < 0) {
                battleLast = loadBattle(1);

                if (battleLast == null) {
                    return true;
                }

                battleList.add(battleLast);
            }

            indexLast = battleList.size() - 1;

            battleLast = battleList.get(indexLast);

            if (battleLast == null) {
                return result;
            }

            while (battleLast.id < omno.state.rgame.arena.battleCount) {

                battleLast = loadBattle(battleLast.id + 1);

                if (battleLast == null) {
                    break;
                }

                battleList.add(battleLast);

                result = true;
            }
        }

        return result;
    }

    public Battle loadBattle(int id) {

        if (id < 1) {
            return null;
        }

        Battle result = Battle.loadBattle(omno, id);

        if (result != null) {
            return result;
        }

        result = remoteApi.getBattle(id);

        if (result != null && result.id == id) {
            return result;
        }

        return null;
    }

    public List<Battle> getBattleList() {

        if (battleList.size() == 0) {
            return null;
        }

        return battleList;
    }
}
