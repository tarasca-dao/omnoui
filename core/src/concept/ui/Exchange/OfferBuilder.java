package concept.ui.Exchange;

import concept.omno.object.PlatformToken;

public class OfferBuilder {

    final PlatformToken give = new PlatformToken();
    final PlatformToken take = new PlatformToken();

    OfferBuilder() {}

    OfferBuilder(PlatformToken give, PlatformToken take) {

        this.give.merge(give, true);
        this.give.merge(take, true);
    }

    public boolean isValid() {
        return !give.isZero() && !take.isZero() && give.isValid() && take.isValid();
    }

    public void zero() {
        give.zero();
        take.zero();
    }

    public void giveAdd(PlatformToken platformToken) {
        give.merge(platformToken, true);
    }

    public void giveSubtract(PlatformToken platformToken) {
        give.merge(platformToken, false);
    }

    public void takeAdd(PlatformToken platformToken) {
        take.merge(platformToken, true);
    }

    public void takeSubtract(PlatformToken platformToken) {
        take.merge(platformToken, false);
    }

    public PlatformToken getGiveFilter() {
        return give.getAsId();
    }

    public PlatformToken getTakeFilter() {
        return take.getAsId();
    }
}
