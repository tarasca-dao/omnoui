package concept.ui.Exchange;

import concept.omno.ApplicationContext;
import concept.omno.object.PlatformToken;
import concept.omno.service.PlatformTokenExchangeById.Offer;

import java.util.ArrayList;
import java.util.List;

public class Data {

    final ApplicationContext omno;

    public final List<Offer> offerList = new ArrayList<>();

    public final PlatformToken takeFilter = new PlatformToken();

    public final PlatformToken give = new PlatformToken();
    public final PlatformToken take = new PlatformToken();

    public final PlatformToken userBalance = new PlatformToken();
    public final long accountId;

    public boolean isGive = false;
    public double multiplier = 1;

    public Data(ApplicationContext applicationContext, long accountId) {
        this.accountId = accountId;
        this.omno = applicationContext;
        initializeTakeFilter();
    }

    public void setMultiplier(long multiplier) {

        if (multiplier < 0) {
            isGive = true;
            this.multiplier = -multiplier;
        } else {
            this.multiplier = multiplier;
        }

    }

    public void setMultiplier(double multiplier) {

        if (multiplier < 0) {
            isGive = true;
            multiplier = -multiplier;
        }

        this.multiplier = multiplier;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void initializeTakeFilter() {

        if (omno == null || !omno.isConfigured) {
            return;
        }

        setFilter(takeFilter, omno.state.rgame.getTokenIds());
    }

    private void setFilter(PlatformToken filter, PlatformToken platformToken) {

        if (platformToken != null && !platformToken.isValid()) {
            return;
        }

        filter.zero();

        if (platformToken == null) {
            return;
        }

        filter.mergeAsId(platformToken);
    }

    public void setFilterTake(PlatformToken platformToken) {
        setFilter(takeFilter, platformToken);
    }

    public PlatformToken getTakeFilter() {
        return takeFilter;
    }

    private void setOfferList(List<Offer> offerList) {

        this.offerList.clear();

        if (offerList == null || offerList.size() == 0) {
            return;
        }

        this.offerList.addAll(offerList);
    }

    public PlatformToken getGive() {
        return give;
    }

    public PlatformToken getTake() {
        return take;
    }

    public PlatformToken getUserBalance() {

        PlatformToken result = userBalance.clone();
        result.keepById(takeFilter);

        return result;
    }

    private void mergePlatformTokenId(PlatformToken target, PlatformToken platformTokenId, boolean isAdd, boolean isBalanceLimited) {

        if (target == null || platformTokenId == null) {
            return;
        }

        if (isAdd) {
            PlatformToken multiplied = platformTokenId.clone();
            multiplied.multiply(multiplier);

            if (isBalanceLimited && !userBalance.isGreaterOrEqual(multiplied)) {
                long value = userBalance.getValueByUniqueId(platformTokenId);
                multiplied = platformTokenId.clone();
                multiplied.multiply(value);
            }

            target.merge(multiplied, true);
        } else {
            target.removeById(platformTokenId);
        }
    }

    public void mergePlatformTokenId(PlatformToken platformToken, boolean isAdd, boolean isBalanceLimited) {

        if (platformToken == null || !platformToken.isValid() || platformToken.isZero()) {
            return;
        }

        PlatformToken target = take;

        if (isGive) {
            target = give;
        }

        mergePlatformTokenId(target, platformToken, isAdd, isBalanceLimited);
    }

    public void mergePlatformTokenIdGive(PlatformToken platformToken, boolean isAdd) {
        mergePlatformTokenId(give, platformToken, isAdd, true);
    }

    public void mergePlatformTokenIdTake(PlatformToken platformToken, boolean isAdd) {
        mergePlatformTokenId(take, platformToken, isAdd, false);
    }

    public void mergePlatformTokenIdTakeFilter(PlatformToken platformToken, boolean isAdd) {
        mergePlatformTokenId(takeFilter, platformToken, isAdd, false);
    }

    private void togglePlatformTokenId(PlatformToken target, PlatformToken platformTokenId, boolean isBalanceLimited) {

        if (target == null || platformTokenId == null || !platformTokenId.isValid() || platformTokenId.isZero() || platformTokenId.countUniqueTokensAll() != 1) {
            return;
        }

        if (target.isGreaterOrEqual(platformTokenId)) {
            target.removeById(platformTokenId);
        } else {
            mergePlatformTokenId(target, platformTokenId, true, isBalanceLimited);
        }
    }

    public void togglePlatformTokenIdGive(PlatformToken platformTokenId) {
        togglePlatformTokenId(give, platformTokenId, true);
    }

    public void togglePlatformTokenIdTake(PlatformToken platformTokenId) {
        togglePlatformTokenId(take, platformTokenId, false);
    }

    public boolean isValidOffer() {

        if (!give.isValid() || give.isZero() || !take.isValid() || take.isZero()) {
            return false;
        }

        Offer offer = new Offer(accountId, give, take, (long) multiplier);

        if (!offer.isValid(omno.state.nativeAssetState)) {
            return false;
        }

        PlatformToken giveMultiplied = give.clone();
        giveMultiplied.multiply(multiplier);

        return userBalance.isGreaterOrEqual(giveMultiplied);
    }

    public boolean update() {

        List<Offer> offerListNew;
        PlatformToken balanceNew;

        synchronized (omno) {

            offerListNew = omno.state.platformTokenExchangeById.getListOfferClone();

            balanceNew = omno.state.getUserAccountBalance(accountId);
        }

        userBalance.zero();
        userBalance.merge(balanceNew, true);

        if (offerListNew == null) {
            offerList.clear();
            return true;
        }

        int offerListNewSize = offerListNew.size();

        if (offerListNewSize != offerList.size()) {
            setOfferList(offerListNew);
            return true;
        }

        for (int i = 0; i < offerListNewSize; i++) {

            if (offerListNew.get(i).id != offerList.get(i).id) {
                setOfferList(offerListNew);
                return true;
            }
        }

        return false;
    }

    public List<Offer> getOfferListFiltered() {

        List<Offer> offerListFiltered = new ArrayList<>();

        if (offerList.size() == 0) {
            return offerListFiltered;
        }

        if (!give.isValid() || !take.isValid() || (give.isZero()) && take.isZero()) {
            offerListFiltered.addAll(offerList);
            return offerListFiltered;
        }

        PlatformToken giveId = give.getAsId();
        PlatformToken takeId = take.getAsId();

        for (Offer item : offerList) {

            if (!item.take.isGreaterOrEqual(giveId)) {
                continue;
            }

            if (!item.give.isGreaterOrEqual(takeId)) {
                continue;
            }

            offerListFiltered.add(item);
        }

        return offerListFiltered;
    }
}
