package concept.ui;

import concept.omno.ApplicationContext;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Arena;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Leaderboard {

    final ApplicationContext applicationContext;

    final HashMap<Long, List<Arena>> mapAccountArena = new HashMap<>();
    final List<List<Arena>> sortListAccountArena = new ArrayList<>();

    public Leaderboard(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public int getSize() {
        return mapAccountArena.size();
    }

    private void clear() {
        mapAccountArena.clear();
        sortListAccountArena.clear();
    }

    public boolean update() {

        clear();

        boolean result = false;

        List<Arena> list = applicationContext.state.rgame.getArenaList();

        if (list == null || list.size() == 0) {
            return false;
        }

        for (Arena item : list) {

            if (item == null || !item.isValid() || item.defender == null) {
                continue;
            }

            addArena(item);

            result = true;
        }

        return result;
    }

    public List<List<Arena>> getSortedListAccountArena() {

        if (sortListAccountArena.size() != 0) {
            return sortListAccountArena;
        }

        update();

        if (getSize() == 0) {
            return null;
        }

        List<Long> listAccountArenaCount = new ArrayList<>();

        for (long accountId : mapAccountArena.keySet()) {

            long count = mapAccountArena.get(accountId).size();

            if (count == 0) {
                continue;
            }

            listAccountArenaCount.add(accountId);
            listAccountArenaCount.add(count);
        }

        int counter = listAccountArenaCount.size();

        if (counter == 0) {
            return null;
        }

        boolean modified = true;

        while (modified) {

            modified = false;

            for (int i = 0; i < counter - 2; i += 2) {

                long count = listAccountArenaCount.get(i + 1);
                long countNext = listAccountArenaCount.get(i + 3);

                if (countNext > count) {
                    long account = listAccountArenaCount.get(i);
                    long accountNext = listAccountArenaCount.get(i + 2);

                    listAccountArenaCount.set(i, accountNext);
                    listAccountArenaCount.set(i + 1, countNext);

                    listAccountArenaCount.set(i + 2, account);
                    listAccountArenaCount.set(i + 3, count);

                    modified = true;
                }
            }
        }

        List<List<Arena>> result = new ArrayList<>();

        for (int i = 0; i <= counter - 2; i += 2) {

            long account = listAccountArenaCount.get(i);

            result.add(mapAccountArena.get(account));
        }

        return result;
    }

    private void addArena(Arena arena) {

        if (arena == null || !arena.isValid()) {
            return;
        }

        long accountId = arena.defender.account;

        if (accountId == 0) {
            return;
        }

        List<Arena> arenaList;

        if (mapAccountArena.containsKey(accountId)) {
            arenaList = mapAccountArena.get(accountId);
        } else {
            arenaList = new ArrayList<>();
            mapAccountArena.put(accountId, arenaList);
        }

        arenaList.add(arena);
    }
}
