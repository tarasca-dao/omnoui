package concept.ui;

import concept.omno.OmnoApi;
import concept.omno.object.PlatformToken;
import concept.platform.ArdorApi;
import concept.platform.EconomicCluster;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Arena;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Army;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Battle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ArenaStaging {

    private final long accountId;
    private final concept.omno.ApplicationContext omno;
    private final ArdorApi ardorApi;
    private final OmnoApi omnoApi;

    public final Arena arena;
    final List<Long> listAttackerTokens = new ArrayList<>();

    public ArenaStaging(concept.omno.ApplicationContext omno, Arena arena, long accountId, ArdorApi ardorApi, OmnoApi omnoApi) {

        this.accountId = accountId;
        this.omno = omno;
        this.ardorApi = ardorApi;
        this.omnoApi = omnoApi;
        this.arena = arena;
    }

    public ArenaStaging(ArenaStaging arenaStaging, Arena arena) {

        this(arenaStaging.omno, arena, arenaStaging.accountId, arenaStaging.ardorApi, arenaStaging.omnoApi);

        for (long item : arenaStaging.listAttackerTokens) {
            addAssetToken(item);
        }
    }

    public boolean isValid() {
        return arena != null;
    }

    public boolean canAddToken(PlatformToken platformTokenId) {

        if (platformTokenId == null || !platformTokenId.isValid() || platformTokenId.isZero()) {
            return false;
        }

        long assetTokenId = platformTokenId.getUniqueAssetTokenId();

        return canAddAssetToken(assetTokenId);
    }

    public boolean canAddAssetToken(long assetTokenId) {

        if (assetTokenId == 0 || (arena.armyRequireUnique && listAttackerTokens.contains(assetTokenId))) {
            return false;
        }

        List<Long> listAsset = new ArrayList<>(listAttackerTokens);
        listAsset.add(assetTokenId);

        return omno.state.rgame.isIncompleteArmyValid(arena.id, accountId, listAsset);
    }

    public boolean addAssetToken(long assetTokenId) {

        if (assetTokenId == 0) {
            return false;
        }

        PlatformToken platformToken = new PlatformToken();
        platformToken.mergeAssetToken(assetTokenId, 1, true);

        return addToken(assetTokenId);
    }

    public boolean removeAssetToken(long assetTokenId) {

        if (assetTokenId == 0 || listAttackerTokens.size() == 0) {
            return false;
        }

        for (int i = 0; i < listAttackerTokens.size(); i++) {

            if (listAttackerTokens.get(i) == assetTokenId) {
                listAttackerTokens.remove(i);
                return true;
            }
        }

        return false;
    }

    public boolean addToken(long assetTokenId) {

        if (assetTokenId == 0 || ! canAddAssetToken(assetTokenId)) {
            return false;
        }

        listAttackerTokens.add(assetTokenId);

        return true;
    }

    public boolean toggleAssetToken(long assetTokenId) {

        if (assetTokenId == 0) {
            return false;
        }

        if (canAddAssetToken(assetTokenId)) {
            return addAssetToken(assetTokenId);
        } else {
            return removeAssetToken(assetTokenId);
        }
    }

    public List<Long> getValidAssetTokens(PlatformToken platformToken) {

        if (platformToken == null || !platformToken.isValid() || platformToken.isZero()) {
            return null;
        }

        HashMap<Long, Long> hashMap = platformToken.getAssetTokenMap();

        if (hashMap == null || hashMap.size() == 0) {
            return null;
        }

        List<Long> result = new ArrayList<>();

        for (long id : hashMap.keySet()) {

            if (!canAddAssetToken(id)) {
                continue;
            }

            result.add(id);
        }

        return result;
    }

    public boolean clear() {

        listAttackerTokens.clear();
        return true;
    }

    public List<Long> getDefenderSoldierAssets() {

        if (arena == null || !arena.isValid() || arena.defender == null || !arena.defender.isValid(true)) {
            return null;
        }

        return new ArrayList<>(arena.defender.getSoldierAssetsIds());
    }

    public List<Long> getAttackerSoldierAssets() {

        if (listAttackerTokens.size() == 0) {
            return null;
        }

        return new ArrayList<>(listAttackerTokens);
    }

    public PlatformToken getBattleCost() {

        if (arena == null || ! arena.isValid()) {
            return null;
        }

        return arena.getBattleCost();
    }

    public Battle getBattle() {

        if (listAttackerTokens.size() == 0) {
            return null;
        }

        Army attacker = getArmyAttacker();


        Battle battle;

        battle = omno.state.rgame.createBattle(arena.id, omno.state.economicCluster.clone(), getArmyDefender(), attacker);

        if (battle == null || !battle.isValidPreBattle(true)) {
            return null;
        }

        return battle;
    }

    public Army getArmyDefender() {

        if (arena == null) {
            return null;
        }

        return arena.defender;
    }

    public Army getArmyAttacker() {

        if (listAttackerTokens.size() == 0) {
            return null;
        }

        return omno.state.rgame.createArmy(arena.id, accountId, getAttackerSoldierAssets());
    }

    public boolean isReady() {

        Army army = getArmyAttacker();

        return army != null && army.isValid(true);
    }

    private void updateEconomicCluster() {
        omnoApi.setEconomicCluster(new EconomicCluster(ardorApi, omno.platformContext.getHeight() - 722));
    }

    public void formArmy() {

        if (omnoApi == null || !isReady()) {
            return;
        }

        updateEconomicCluster();
        omnoApi.formArmy(arena.id, listAttackerTokens);
    }
}
