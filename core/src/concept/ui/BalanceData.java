package concept.ui;

import concept.omno.OmnoApi;
import concept.omno.object.PlatformToken;
import concept.platform.ArdorApi;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class BalanceData {

    private final ArdorApi ardorApi;
    public final OmnoApi omnoApi;

    private final PlatformToken platform = new PlatformToken();
    private final PlatformToken omno = new PlatformToken();
    public final PlatformToken deposit = new PlatformToken();
    public final PlatformToken withdraw = new PlatformToken();
    public boolean isDeposit = true;
    public double multiplier = 1;

    PlatformToken tokenFilter;

    public BalanceData(ArdorApi ardorApi, OmnoApi omnoApi, PlatformToken tokenFilter) {
        this.ardorApi = ardorApi;
        this.omnoApi = omnoApi;
        this.tokenFilter = tokenFilter;
    }

    public void setMultiplier(long multiplier) {

        if (multiplier < 0) {
            isDeposit = false;
            multiplier = -multiplier;
        }

        this.multiplier = multiplier;
    }

    public void setMultiplier(double multiplier) {

        if (multiplier < 0) {
            isDeposit = false;
            multiplier = -multiplier;
        }

        this.multiplier = multiplier;
    }

    public PlatformToken getOmno() {
        return omno;
    }

    public PlatformToken getPlatform() {
        return platform;
    }

    public void omnoZero() {
        omno.zero();
    }

    public void platformZero() {
        platform.zero();
    }

    private void filter() {

        if (tokenFilter != null && tokenFilter.isValid()) {
            omno.keepById(tokenFilter);
            platform.keepById(tokenFilter);
        }
    }

    public void setTokenFilter(PlatformToken platformToken) {
        this.tokenFilter = platformToken;
        filter();
    }

    private void merge(PlatformToken target, PlatformToken platformToken, boolean isAdd) {

        if (platformToken == null || !platformToken.isValid() || platformToken.isZero() || target == null) {
            return;
        }

        PlatformToken platformTokenClone = platformToken.clone();

        if (tokenFilter != null && tokenFilter.isValid()) {
            platformTokenClone.keepById(tokenFilter);
        }

        target.merge(platformTokenClone, true);
    }

    public void omnoMerge(PlatformToken platformToken, boolean isAdd) {
        merge(omno, platformToken, isAdd);
    }

    public void platformMerge(PlatformToken platformToken, boolean isAdd) {
        merge(platform, platformToken, isAdd);
    }

    public void broadcast() {

        if (omnoApi == null) {
            return;
        }

        if (deposit.isValid() && !deposit.isZero()) {
            omnoApi.deposit(deposit);
            deposit.zero();
        }

        if (withdraw.isValid() && !withdraw.isZero()) {
            omnoApi.withdraw(withdraw);
            withdraw.zero();
        }
    }

    public void reset() {
        deposit.zero();
        withdraw.zero();
    }

    public void depositAll(PlatformToken transactionFee) {

        if (omnoApi == null) {
            return;
        }

        if (platform.isValid() && !platform.isZero()) {
            PlatformToken platformClone = platform.clone();
            platformClone.applyTransactionFee(transactionFee, 2, false);
            platformClone.removeChainTokens();
            omnoApi.deposit(platformClone);
            deposit.zero();
        }
    }

    public void withdrawAll() {

        if (omnoApi == null) {
            return;
        }

        if (omno.isValid() && !omno.isZero()) {
            omnoApi.withdrawAll();
            withdraw.zero();
        }
    }

    public void mergeUnitUnique(PlatformToken delta) {

        if (delta == null || delta.countUniqueTokensAll() != 1) {
            return;
        }

        PlatformToken deltaMultiplied = delta.clone();
        deltaMultiplied.multiply(multiplier * deltaMultiplied.getUnitValueByUnique(ardorApi));

        PlatformToken platformCombined = platform.clone();
        platformCombined.merge(deposit, false);
        platformCombined.merge(withdraw, true);

        PlatformToken omnoCombined = omno.clone();
        omnoCombined.merge(withdraw, false);
        omnoCombined.merge(deposit, true);

        if (isDeposit) {
            if (platformCombined.isGreaterOrEqual(deltaMultiplied)) {
                withdraw.subtractLimitedWithRemainder(deltaMultiplied);
                deposit.merge(deltaMultiplied, true);
            }
        } else {
            if (omnoCombined.isGreaterOrEqual(deltaMultiplied)) {
                deposit.subtractLimitedWithRemainder(deltaMultiplied);
                withdraw.merge(deltaMultiplied, true);
            }
        }
    }

    public String getOmnoAssetTokenValueAsUnitString(long id, PlatformToken delta) {
        return getAssetTokenValueAsUnitString(omno, delta, id);
    }

    public String getPlatformAssetTokenValueAsUnitString(long id, PlatformToken delta) {
        return getAssetTokenValueAsUnitString(platform, delta, id);
    }

    public String getDepositAssetTokenValueAsUnitString(long id, PlatformToken delta) {
        return getAssetTokenValueAsUnitString(deposit, delta, id);
    }

    public String getWithdrawAssetTokenValueAsUnitString(long id, PlatformToken delta) {
        return getAssetTokenValueAsUnitString(withdraw, delta, id);
    }

    public String getOmnoChainTokenValueAsUnitString(long id, PlatformToken delta) {
        return getChainTokenValueAsUnitString(omno, delta, id);
    }

    public String getPlatformChainTokenValueAsUnitString(long id, PlatformToken delta) {
        return getChainTokenValueAsUnitString(platform, delta, id);
    }

    public String getDepositChainTokenValueAsUnitString(long id, PlatformToken delta) {
        return getChainTokenValueAsUnitString(deposit, delta, id);
    }

    public String getWithdrawChainTokenValueAsUnitString(long id, PlatformToken delta) {
        return getChainTokenValueAsUnitString(withdraw, delta, id);
    }

    private String getAssetTokenValueAsUnitString(PlatformToken balance, PlatformToken delta, long id) {

        if (balance == null || balance.isZero() || id == 0) {
            return "0";
        }

        long value = balance.getAssetTokenValue(id);

        if (delta != null) {
            value -= delta.getAssetTokenValue(id);
        }

        try {
            double doubleValue = (double) value / ardorApi.getAsset(id).getUnit();
            return string(doubleValue);
        } catch (Exception e) {
            return "0";
        }
    }

    private String getChainTokenValueAsUnitString(PlatformToken balance, PlatformToken delta, long id) {

        if (balance == null || balance.isZero() || id == 0) {
            return "0";
        }

        long value = balance.getChainTokenValue(id);

        if (delta != null) {
            value -= delta.getChainTokenValue(id);
        }

        try {
            double doubleValue = (double) value / ardorApi.getChainToken((int) id).ONE_COIN;
            return string(doubleValue);
        } catch (Exception e) {
            return "0";
        }
    }

    private String string(double doubleValue) {

        if (doubleValue == 0) {
            return "0";
        }

        if (doubleValue >= 1) {
            return Long.toUnsignedString((long) doubleValue);
        }

        NumberFormat numberFormat;

        if (doubleValue >= 1.0) {
            numberFormat = new DecimalFormat("#.0");
        } else {
            numberFormat = new DecimalFormat("#.00000000");
        }

        return numberFormat.format(doubleValue);
    }
}
