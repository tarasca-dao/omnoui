package concept.ui;

import concept.omno.object.PlatformToken;
import concept.platform.ArdorApi;
import concept.platform.EconomicCluster;
import concept.utility.NxtCryptography;

import java.util.HashMap;

public class UserAccount {
    public NxtCryptography nxtCryptography;
    ArdorApi ardorApi;
    EconomicCluster economicCluster;
    public HashMap<Integer, ArdorApi.ChainTokenBalance> chainToken;
    public HashMap<Long, ArdorApi.AssetTokenBalance> assetToken;

    public PlatformToken platformBalance = new PlatformToken();

    public UserAccount(NxtCryptography nxtCryptography, ArdorApi ardorApi) {
        this.nxtCryptography = nxtCryptography;
        this.ardorApi = ardorApi;
    }

    public boolean isValid() {
        return (economicCluster != null && economicCluster.isValid(ardorApi) && nxtCryptography != null && ardorApi != null);
    }

    public void balanceSync(EconomicCluster economicCluster, boolean firstRun) {

        if (economicCluster == null || !economicCluster.isValid(ardorApi) || (this.economicCluster != null && this.economicCluster.isEqual(economicCluster))) {
            return;
        }

        this.economicCluster = economicCluster.clone();

        assetToken = ardorApi.getAccountAssets(nxtCryptography.getAccountId());

        platformBalance = new PlatformToken();
        platformBalance.merge(assetToken, true);

        chainToken = new HashMap<>();
        ArdorApi.ChainTokenBalance chainTokenBalance;

        chainTokenBalance = ardorApi.getBalance(nxtCryptography.getAccountId(), 2);

        if (chainTokenBalance != null) {
            chainToken.put(2, chainTokenBalance);
            platformBalance.mergeChainToken(2, chainTokenBalance.balanceNQT, true);
        }

        chainTokenBalance = ardorApi.getBalance(nxtCryptography.getAccountId(), 1);

        if (chainTokenBalance != null) {
            chainToken.put(1, chainTokenBalance);
            platformBalance.mergeChainToken(1, chainTokenBalance.balanceNQT, true);
        }
    }
}
