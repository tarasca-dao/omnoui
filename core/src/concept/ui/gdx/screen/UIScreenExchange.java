package concept.ui.gdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import concept.omno.OmnoApi;
import concept.omno.object.PlatformToken;
import concept.omno.service.PlatformTokenExchangeById.Offer;
import concept.platform.EconomicCluster;
import concept.ui.Exchange.Data;
import concept.ui.gdx.ApplicationContext;
import concept.ui.gdx.TransactionNotifier;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UIScreenExchange extends InputAdapter implements Screen, Runnable {

    private final ApplicationContext applicationContext;
    private final OmnoApi omnoApi;
    private final concept.omno.ApplicationContext omno;
    private final Stage stage;
    private final OrthographicCamera camera;
    private final Skin skin;

    final TransactionNotifier transactionNotifier;

    final concept.ui.Exchange.Data exchangeData;
    final Table tableOffers;

    final TextField textFieldValue;
    final Table tableGiveOrFilter;
    final Table tableTake;
    final ScrollPane scrollPaneTableGiveOrFilter;
    final TextButton buttonOfferIssue;

    EconomicCluster economicCluster;
    private boolean stop = false;
    private boolean stopComplete = true;

    public void stop() {
        stop = true;
    }

    @Override
    public void run() {

        stopComplete = false;

        while(!stop) {
            synchronized (this) {

                transactionNotifier.update();
                transactionNotifier.updateVisibility();

                EconomicCluster economicCluster = applicationContext.getEconomicClusterClone();

                if (this.economicCluster == null || ! this.economicCluster.isEqual(economicCluster)) {
                    this.economicCluster = economicCluster;

                    processBlock();
                }
            }

            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException ignored) {}
        }

        stopComplete = true;
    }

    public boolean waitStopComplete() {

        if (stop) {

            while (!stopComplete) {
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException ignored) {}
            }
        }

        return stop;
    }

    public UIScreenExchange(ApplicationContext applicationContext) {

        this.applicationContext = applicationContext;
        omnoApi = applicationContext.getOmnoApi();
        omno = applicationContext.omno;
        exchangeData = new Data(omno, applicationContext.getUserAccountId());
        transactionNotifier = new TransactionNotifier(applicationContext);

        synchronized (this.applicationContext) {
            camera = new OrthographicCamera();
            camera.setToOrtho(false, applicationContext.width, applicationContext.height);
            stage = new Stage(new FitViewport(applicationContext.width, applicationContext.height, camera));
            skin = applicationContext.skin;
        }

        tableOffers = new Table();
        tableGiveOrFilter = new Table();
        tableTake = new Table();
        Table tablePaneLeft = new Table();
        Table tablePaneRight = new Table();
        tablePaneLeft.defaults().pad(5);
        ScrollPane scrollPane = new ScrollPane(tableOffers);
        scrollPane.setWidth(200);
        scrollPane.setScrollingDisabled(true, false);
        tablePaneRight.add(scrollPane);
        tableOffers.setWidth(200);

        Table root = new Table();
        root.setFillParent(true);
        root.defaults().pad(5);
        stage.addActor(root);

        root.add(tablePaneLeft).width(1200);
        root.add(tablePaneRight);

        Table table;

        table = new Table();
        table.defaults().pad(10.0f);

        table.add(applicationContext.createButtonLogout());
        table.add(applicationContext.createButtonBalance());
        table.add(applicationContext.createButtonArena());
        table.add(applicationContext.createButtonLeaderBoard());

        table.add(transactionNotifier.getTable());

        tablePaneLeft.add(table);

        tablePaneLeft.row();

        table = new Table();

        Texture texture;
        Drawable drawable;

        texture = applicationContext.textureCache.getNamedTexture("arrowuncheckedup", "small");
        Texture textureChecked = applicationContext.textureCache.getNamedTexture("arrowup", "small");

        drawable = new TextureRegionDrawable(texture);
        Drawable drawableChecked = new TextureRegionDrawable(textureChecked);
        ImageButton imageButtonGive = new ImageButton(drawable, drawable, drawableChecked);
        ImageButton imageButtonTake = new ImageButton(drawable, drawable, drawableChecked);
        final boolean[] mutex = {false};

        imageButtonGive.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                if (!mutex[0]) {

                    mutex[0] = true;

                    imageButtonGive.setChecked(true);

                    if (imageButtonTake.isChecked()) {
                        imageButtonTake.setChecked(false);
                    }

                    mutex[0] = false;

                    synchronized (applicationContext) {
                        exchangeData.isGive = true;
                        updateTableGiveAsBalance();
                        updateTextFieldValue();
                    }
                }
            }
        });

        table.add(imageButtonGive);

        imageButtonTake.setTransform(true);
        imageButtonTake.setOrigin(imageButtonTake.getWidth() / 2, imageButtonTake.getHeight() / 2);
        imageButtonTake.rotateBy(180);

        imageButtonTake.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                if (!mutex[0]) {

                    mutex[0] = true;

                    imageButtonTake.setChecked(true);

                    if (imageButtonGive.isChecked()) {
                        imageButtonGive.setChecked(false);
                    }

                    mutex[0] = false;

                    synchronized (applicationContext) {
                        exchangeData.isGive = false;
                        updateTableGiveAsFilterTake();
                        updateTextFieldValue();
                    }
                }
            }
        });

        table.add(imageButtonTake);

        addMultiplierButton(table, 1);
        addMultiplierButton(table, 10);
        addMultiplierButton(table, 100);
        addMultiplierButton(table, 1000);

        textFieldValue = new TextField("", skin);

        textFieldValue.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                synchronized (this) {

                    String text = textFieldValue.getText();

                    double doubleValue;

                    if (text != null && text.length() != 0) {

                        try {
                            doubleValue = Double.parseDouble(text);
                        } catch (Exception e) {
                            doubleValue = 1;
                        }

                        exchangeData.setMultiplier(doubleValue);
                    }
                }
            }
        });

        stage.setKeyboardFocus(textFieldValue);

        table.add(textFieldValue).expandX().colspan(12);

        buttonOfferIssue = createButtonOfferIssue();
        buttonOfferIssue.setVisible(false);
        table.add(buttonOfferIssue);

        tablePaneLeft.add(table);

        imageButtonTake.setChecked(true);

        tablePaneLeft.row();
        tablePaneLeft.add(new ScrollPane(tableTake));
        tablePaneLeft.row();
        scrollPaneTableGiveOrFilter = new ScrollPane(tableGiveOrFilter);
        tablePaneLeft.add(scrollPaneTableGiveOrFilter);

        updateTableGiveAsBalance();
        updateTableTake();
        updateTableGiveAsFilterTake();
        updateTextFieldValue();
        processBlock();
        Thread thread = new Thread(this);
        thread.start();
    }

    private void updateTextFieldValue() {
        synchronized (this) {

            double multiplier = exchangeData.multiplier;

            if (exchangeData.isGive) {
                multiplier = -multiplier;
            }

            textFieldValue.setText(Double.toString(multiplier));
        }

        setButtonOfferIssueVisibility();
    }

    private void addMultiplierButton(Table table, long multiplier) {

        TextButton textButton = new TextButton(Long.toUnsignedString(multiplier), skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                exchangeData.setMultiplier(multiplier);
                updateTextFieldValue();
            }
        });

        table.add(textButton);
    }

    private boolean updateTableOffers(boolean forceUpdate) {

        boolean updated = exchangeData.update();

        if (!forceUpdate && !updated) {
            return false;
        }

        tableOffers.clear();

        List<Offer> offerList = exchangeData.getOfferListFiltered();

        if (offerList.size() == 0) {
            return true;
        }

        PlatformToken userBalance = applicationContext.getOmnoUserAccountBalance();

        if (userBalance == null) {
            return true;
        }

        long userAccount = applicationContext.getUserAccountId();

        for (Offer item : offerList) {

            if (!applicationContext.isReadOnly() && item.account != userAccount && !userBalance.isGreaterOrEqual(item.take)){
                continue;
            }

            Table table = createTableOffer(item);

            if (table == null) {
                continue;
            }

            tableOffers.add(table);
            tableOffers.row();
        }

        return true;
    }

    private Table createTableOffer(Offer offer) {

        if (offer == null) {
            return null;
        }

        Table result = new Table();
        result.defaults().pad(5);

        Table tableInner1 = new Table();
        tableInner1.defaults().pad(10);
        result.add(tableInner1);

        tableInner1.add(new Label("# ".concat(Long.toUnsignedString(offer.id)), skin));

        if (offer.account == applicationContext.getUserAccountId()) {
            if (!applicationContext.isReadOnly()) {
                tableInner1.add(createButtonOfferCancel(offer.id));
            }
        } else {
            if (!applicationContext.isReadOnly()) {
                tableInner1.add(createButtonOfferAccept(offer.id));
                tableInner1.row();
            }
            result.row();
            result.add(new Label(applicationContext.getArdorApi().getAccountRSAbbreviated(offer.account), skin));
        }

        tableOfferAddPlatformToken(result, offer.give);
        result.row();
        result.add(applicationContext.textureCache.getImageNamed("arrowup", "icon", 180));
        result.row();
        result.add(applicationContext.textureCache.getImageNamed("arrowup", "icon", 0));
        tableOfferAddPlatformToken(result, offer.take);

        return result;
    }

    private void tableOfferAddPlatformToken(Table root, PlatformToken platformToken) {

        if (platformToken == null || platformToken.isZero() || root == null) {
            return;
        }

        tableOfferAddMapTokens(root, platformToken.getChainTokenMap(), false);
        tableOfferAddMapTokens(root, platformToken.getAssetTokenMap(), true);
    }

    private void tableOfferAddMapTokens(Table root, HashMap<Long, Long> hashMap, boolean isAssetToken) {

        if (hashMap == null || hashMap.size() == 0) {
            return;
        }

        for (long key : hashMap.keySet()) {

            long value = hashMap.get(key);

            if (value == 0) {
                continue;
            }

            Table table = createTableToken(key, value, isAssetToken, "small");

            root.row();
            root.add(table);
        }
    }

    private Table createTableToken(long id, long value, boolean isAsset, String type) {

        if (id == 0 || value == 0 || type == null) {
            return null;
        }

        Table result = new Table();

        tableAddToken(result, id, isAsset, type);

        Label label;

        // show token ID #
        if (true) {
            result.row();
            label = new Label("# " + Long.toUnsignedString(id), skin);
            label.setFontScale(1, 1);
            result.add(label);
        }

        double valueScaled;

        if (isAsset) {
            valueScaled = (double) value / applicationContext.getArdorApi().getAsset(id).getUnit();
        } else {
            valueScaled = (double) value / applicationContext.getArdorApi().getChainToken((int) id).ONE_COIN;
        }

        result.row();
        label = new Label(Double.toString(valueScaled), skin);
        label.setFontScale(2, 2);
        result.add(label);

        return result;
    }

    private void updateTableTake() {

        PlatformToken balance = exchangeData.getTake();

        tableTake.clear();

        if (balance == null || !balance.isValid() || balance.isZero()) {
            return;
        }

        HashMap<Long, Long> hashMapToken;

        hashMapToken = balance.getChainTokenMap();
        tableAddTokenBalance(tableTake, hashMapToken, false, exchangeData, false, false, "medium");

        hashMapToken = balance.getAssetTokenMap();
        tableAddTokenBalance(tableTake, hashMapToken, true, exchangeData, false, false, "medium");

        tableTake.add(applicationContext.textureCache.getImageNamed("arrowup", "small", 180));
    }

    private void updateTableGive() {

        if (exchangeData.isGive) {
            updateTableGiveAsBalance();
        } else {
            updateTableGiveAsFilterTake();
        }
    }

    private void updateTableGiveAsBalance() {

        PlatformToken balance = exchangeData.getGive();

        HashMap<Long, Long> hashMapToken;

        tableGiveOrFilter.clear();

        boolean hasGive = false;

        if (balance != null && balance.isValid() && !balance.isZero()) {

            hashMapToken = balance.getChainTokenMap();
            tableAddTokenBalance(tableGiveOrFilter, hashMapToken, false, exchangeData, true, false, "medium");

            hashMapToken = balance.getAssetTokenMap();
            tableAddTokenBalance(tableGiveOrFilter, hashMapToken, true, exchangeData, true, false, "medium");

            hasGive = true;
        }

        if (hasGive) {
            tableGiveOrFilter.add(applicationContext.textureCache.getImageNamed("arrowup", "small", 0));
        }

        PlatformToken balanceUser = exchangeData.getUserBalance();

        if (balanceUser == null) {
            return;
        }

        balanceUser = balanceUser.clone();

        balanceUser.removeById(balance);

        hashMapToken = balanceUser.getChainTokenMap();
        tableAddTokenBalance(tableGiveOrFilter, hashMapToken, false, exchangeData, true, false, "medium");

        hashMapToken = balanceUser.getAssetTokenMap();
        tableAddTokenBalance(tableGiveOrFilter, hashMapToken, true, exchangeData, true, false, "medium");
    }

    private void updateTableGiveAsFilterTake() {

        PlatformToken balance = exchangeData.getTakeFilter();

        HashMap<Long, Long> hashMapToken;

        tableGiveOrFilter.clear();

        if (balance != null && balance.isValid() && !balance.isZero()) {

            hashMapToken = balance.getChainTokenMap();
            tableAddTokenBalance(tableGiveOrFilter, hashMapToken, false, exchangeData, true, true, "medium");

            hashMapToken = balance.getAssetTokenMap();
            tableAddTokenBalance(tableGiveOrFilter, hashMapToken, true, exchangeData, true, true, "medium");
        }
    }

    private ImageButton tableAddToken(Table root, long tokenId, boolean isAsset, String type) {

        if (root == null || tokenId == 0) {
            return null;
        }

        Texture texture = applicationContext.textureCache.getTokenTexture(tokenId, type, isAsset);

        Drawable drawable = new TextureRegionDrawable(texture);
        ImageButton result = new ImageButton(drawable);

        root.add(result).width(result.getWidth()).height(result.getHeight());

        return result;
    }

    private void updateTables() {
        updateTableGive();
        updateTableTake();
        updateTableOffers(true);
        setButtonOfferIssueVisibility();
    }

    private void tableAddTokenBalance(Table root, HashMap<Long, Long> mapTokenBalance, boolean isAsset, Data exchangeData, boolean isGive, boolean isFilterTake, String type) {

        if (root == null || mapTokenBalance == null || mapTokenBalance.size() == 0 || exchangeData == null || type == null) {
            return;
        }

        for (long key : mapTokenBalance.keySet()) {

            PlatformToken platformTokenId = new PlatformToken();

            if (isAsset) {
                platformTokenId.mergeAssetToken(key, 1, true);
            } else {
                platformTokenId.mergeChainToken(key, 1, true);
            }

            Table table = new Table();

            ImageButton imageButton = tableAddToken(table, key, isAsset, type);

            if (isFilterTake) {
                imageButton.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {

                        synchronized (this) {
                            exchangeData.mergePlatformTokenIdTake(platformTokenId, true);
                            updateTables();
                        }
                    }
                });
            } else if (isGive) {
                imageButton.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {

                        synchronized (this) {
                            exchangeData.togglePlatformTokenIdGive(platformTokenId);
                            updateTables();
                            scrollPaneTableGiveOrFilter.scrollTo(0,0,0,0);
                        }
                    }
                });
            } else {
                imageButton.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {

                        synchronized (this) {
                            exchangeData.mergePlatformTokenIdTake(platformTokenId, false);
                            updateTables();
                        }
                    }
                });
            }

            Label label;

            // show token ID #
            if (true) {
                table.row();
                label = new Label("# " + Long.toUnsignedString(key), skin);
                label.setFontScale(1, 1);
                table.add(label);
            }

            if (!isFilterTake) {
                long value = mapTokenBalance.get(key);

                table.row();
                table.add(new Label(Long.toUnsignedString(value), skin));
            }

            root.add(table).padRight(10);
        }
    }

    private TextButton createButtonOfferIssue() {

        TextButton result = new TextButton("issue", skin, "small");

        result.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                omnoApi.platformTokenExchangeByIdOperationOffer(exchangeData.getGive(), exchangeData.getTake(), (long) exchangeData.getMultiplier());
            }
        });

        return result;
    }

    private TextButton createButtonOfferAccept(long id) {

        TextButton result = new TextButton("accept", skin, "small");

        result.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                omnoApi.platformTokenExchangeByIdOperationAccept(id, (long) exchangeData.getMultiplier());
            }
        });

        return result;
    }

    private TextButton createButtonOfferCancel(long id) {

        TextButton result = new TextButton("cancel", skin, "small");

        result.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                omnoApi.platformTokenExchangeByIdOperationCancel(id);
            }
        });

        return result;
    }

    private void setButtonOfferIssueVisibility() {
        buttonOfferIssue.setVisible(exchangeData.isValidOffer());
    }

    private void processBlock() {
        updateTableOffers(false);
    }

    @Override
    public void show() {

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

        synchronized (this) {
            applicationContext.defaultScreenClear();
            stage.act(Gdx.graphics.getDeltaTime());
            stage.draw();
        }
    }

    @Override
    public void resize(int width, int height) {

        synchronized (this) {
            stage.getViewport().update(width, height, true);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

        stop();
        waitStopComplete();

        synchronized (this) {
            stage.dispose();
        }
    }
}
