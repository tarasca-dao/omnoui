package concept.ui.gdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import concept.omno.object.PlatformToken;
import concept.omno.object.UserAccount;
import concept.platform.EconomicCluster;
import concept.ui.ArenaStaging;
import concept.ui.gdx.ApplicationContext;
import concept.ui.gdx.GdxScene2dUtility;
import concept.ui.gdx.TransactionNotifier;
import org.tarasca.mythicalbeings.rgame.omno.service.RgameUtility;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Arena;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Battle;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Soldier;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UIScreenArena extends InputAdapter implements Screen, Runnable {

    private final ApplicationContext applicationContext;
    private final Stage stage;
    private final OrthographicCamera camera;
    private final Skin skin;

    final TransactionNotifier transactionNotifier;
    final List<Integer> listArenaId = new ArrayList<>();
    int arenaIndex = 0;
    ArenaStaging arenaStaging;
    final Table arenaDefenderTable;
    final Table arenaAttackerTable;
    final Table arenaInfoTable;
    final Label labelChance;
    final TextButton buttonExecute;

    EconomicCluster economicCluster;
    private boolean stop = false;
    private boolean stopComplete = true;

    public void stop() {
        stop = true;
    }

    @Override
    public void run() {

        stopComplete = false;

        while(!stop) {
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException ignored) { }

            synchronized (this) {

                transactionNotifier.update();
                transactionNotifier.updateVisibility();

                EconomicCluster economicCluster = applicationContext.getEconomicClusterClone();

                if (this.economicCluster == null || ! this.economicCluster.isEqual(economicCluster)) {
                    this.economicCluster = economicCluster;
                    processBlock();
                }
            }
        }

        stopComplete = true;
    }

    public boolean waitStopComplete() {

        if (stop) {

            while (!stopComplete) {
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException ignored) {}
            }
        }

        return stop;
    }

    public UIScreenArena(ApplicationContext applicationContext, int arenaId) {
        this.applicationContext = applicationContext;
        concept.omno.ApplicationContext omno = applicationContext.omno;
        transactionNotifier = new TransactionNotifier(applicationContext);

        synchronized (this.applicationContext) {
            camera = new OrthographicCamera();
            camera.setToOrtho(false, applicationContext.width, applicationContext.height);
            stage = new Stage(new FitViewport(applicationContext.width, applicationContext.height, camera));
            skin = applicationContext.skin;
        }

        arenaDefenderTable = new Table();
        arenaAttackerTable = new Table();
        arenaDefenderTable.defaults().padRight(10);
        arenaAttackerTable.defaults().padRight(10);

        arenaInfoTable = new Table();
        labelChance = new Label("0%", skin);
        buttonExecute = new TextButton("Execute", skin, "small");
        buttonExecute.setVisible(false);

        buttonExecute.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                arenaStaging.formArmy();
                updateBattleTables();
            }
        });

        synchronized (applicationContext.omno) {

            if (omno.isConfigured && omno.state.isValid()) {
                List<Integer> list = omno.state.rgame.getArenaIds();

                if (list != null) {
                    listArenaId.addAll(omno.state.rgame.getArenaIds());
                }
            }

            if (listArenaId.size() > 0 && arenaId >= 0) {

                for (int i = 0; i < listArenaId.size(); i++) {

                    if (listArenaId.get(i) == arenaId) {
                        arenaIndex = i;
                        break;
                    }
                }
            }

            updateArena();
        }

        Table root = new Table();
        root.setFillParent(true);
        stage.addActor(root);

        Table table;

        table = new Table();
        table.defaults().pad(10.0f);

        table.add(applicationContext.createButtonLogout());
        table.add(applicationContext.createButtonBalance());
        table.add(applicationContext.createButtonExchange());
        table.add(applicationContext.createButtonLeaderBoard());

        TextButton textButton;
        textButton = new TextButton("Previous", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                arenaIndex--;

                if (arenaIndex < 0) {
                    arenaIndex = listArenaId.size() - 1;
                }

                updateBattleTables();
            }
        });

        table.add(textButton);

        textButton = new TextButton("Next", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                arenaIndex++;

                if (arenaIndex >= listArenaId.size()) {
                    arenaIndex = 0;
                }

                updateBattleTables();
            }
        });

        table.add(textButton);

        table.add(buttonExecute);
        table.add(transactionNotifier.getTable());

        table.add(labelChance).align(Align.left);

        root.add(table);

        root.row();
        updateTableArenaInfo();
        root.add(arenaInfoTable);

        updateBattleTables();

        root.row();
        root.add(arenaDefenderTable);

        root.row();
        ScrollPane scrollPane = new ScrollPane(arenaAttackerTable);
        root.add(scrollPane);

        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        synchronized (this) {
            applicationContext.defaultScreenClear();
            stage.act(Gdx.graphics.getDeltaTime());
            stage.draw();
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stop();
        waitStopComplete();

        synchronized (this) {
            stage.dispose();
        }
    }

    private void processBlock() {

    }

    private void updateArena() {

        if (arenaIndex < 0 || arenaIndex >= listArenaId.size()) {
            return;
        }

        int arenaId = listArenaId.get(arenaIndex);

        if (arenaStaging != null && arenaStaging.arena != null && arenaStaging.isValid() && arenaStaging.arena.id == arenaId) {
            return;
        }

        Arena arena = applicationContext.omno.state.rgame.getArenaClone(arenaId);

        if (arena == null || !arena.isValid()) {
            return;
        }

        if (arenaStaging == null) {
            arenaStaging = new ArenaStaging(applicationContext.omno, arena, applicationContext.getUserAccountId(), applicationContext.getArdorApi(), applicationContext.getOmnoApi());
        } else {
            arenaStaging = new ArenaStaging(arenaStaging, arena);
        }

        if (arena != null) {

            if (arena.defender.account == applicationContext.getUserAccountId()) {
                buttonExecute.setText("Stack");
            } else {
                buttonExecute.setText("Execute");
            }
        }

        updateTableArenaInfo();

        updateBattleTables();
    }

    private void updateTableArenaInfo() {

        arenaInfoTable.clear();

        if (arenaStaging != null && arenaStaging.isValid()) {
            arenaInfoTable.add(new Label("# " + arenaStaging.arena.id, skin));
            GdxScene2dUtility.tableAddSymbolValue(applicationContext, arenaInfoTable, "snowman", applicationContext.getArdorApi().getAccountRSAbbreviated(arenaStaging.arena.defender.account), 1, 10);
            GdxScene2dUtility.tableAddSymbolValue(applicationContext, arenaInfoTable, "medium".concat(Integer.toString(arenaStaging.arena.mediumId)), 0, 10);
            GdxScene2dUtility.tableAddSymbolValue(applicationContext, arenaInfoTable, "domain".concat(Integer.toString(arenaStaging.arena.domainId)), 0, 10);

            int countRankMaximum =  arenaStaging.arena.armyRankMaximum.size();

            for (int i = 0; i < countRankMaximum; i++) {
                GdxScene2dUtility.tableAddSymbolValue(applicationContext, arenaInfoTable, "rank".concat(Integer.toString(i)), arenaStaging.arena.getRankMaximum(i), 10);
            }

            GdxScene2dUtility.tableAddPlatformToken(applicationContext, arenaInfoTable, arenaStaging.getBattleCost(), 2, 10);
        }
    }

    private boolean updateTableArena(Table table, List<Soldier> soldierList, boolean clear, boolean isDefender) {

        if (clear) {
            table.clear();
        }

        if (soldierList == null || soldierList.size() == 0) {
            return false;
        }

        RgameUtility.sortListSoldier(soldierList);

        boolean result = false;

        for (Soldier item : soldierList) {

            if (isDefender) {
                table.add(createTableDefenderCard(item));
            } else {
                table.add(createTableAttackerCard(item));
            }

            result = true;
        }

        return result;
    }

    private void updateTableArenaDefender(List<Soldier> soldierList) {
        updateTableArena(arenaDefenderTable, soldierList, true, true);
    }

    private void updateTableArenaAttacker(List<Soldier> soldierList) {

        boolean hasPick = updateTableArena(arenaAttackerTable, soldierList, true, false);

        UserAccount userAccount = applicationContext.omno.state.userAccountState.getUserAccount(applicationContext.userAccount.nxtCryptography.getAccountId());

        if (userAccount == null) {
            return;
        }

        PlatformToken balance = userAccount.balance;

        if (balance == null || !balance.isValid() || balance.isZero()) {
            return;
        }

        List<Long> listAssetId = arenaStaging.getValidAssetTokens(balance);

        if (listAssetId == null || listAssetId.size() == 0) {
            return;
        }

        List<Soldier> listRemaining =  applicationContext.omno.state.rgame.listSoldierFromListAssetToken(listAssetId);

        if (listRemaining != null && listRemaining.size() > 0) {

            if (hasPick) {
                Texture texture = applicationContext.textureCache.getNamedTexture("arrowup", "small");
                Image image = new Image(texture);
                arenaAttackerTable.add(image);
            }

            updateTableArena(arenaAttackerTable, listRemaining, false, false);
        }
    }

    private Table createTableAttackerCard(long assetId) {

        if (assetId == 0) {
            return null;
        }

        Soldier soldier = applicationContext.omno.state.rgame.getSoldierCloneByAssetId(assetId);

        return createTableAttackerCard(soldier);
    }

    private Table createTableAttackerCard(Soldier soldier) {

        if (soldier == null || !soldier.isValid()) {
            return null;
        }

        long assetTokenId = soldier.asset;

        Table result = new Table();

        Texture texture = applicationContext.textureCache.getTokenTexture(assetTokenId, "medium", true);

        Drawable drawable = new TextureRegionDrawable(texture);
        ImageButton imageButton = new ImageButton(drawable);

        imageButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                synchronized (this) {
                    arenaStaging.toggleAssetToken(assetTokenId);
                    updateBattleTables();
                }
            }
        });

        result.add(imageButton).width(imageButton.getWidth()).height(imageButton.getHeight());

        if (true) {
            result.row();
            Label label = new Label("# " + Long.toUnsignedString(assetTokenId), skin);
            label.setFontScale(1, 1);
            result.add(label);
        }

        result.row();
        Table table = GdxScene2dUtility.getTableSoldierProperties(applicationContext, soldier);

        result.add(table);

        return result;
    }

    private Table createTableAttackerArmyCard(Battle battle) {

        if (battle == null || !battle.isValidPreBattle(true)) {
            return null;
        }

        long assetTokenId = 0;

        battle.calculateBattle(false);

        Soldier soldier = applicationContext.omno.state.rgame.getSoldierCloneByAssetId(assetTokenId);

        if (soldier == null || !soldier.isValid()) {
            return null;
        }

        Table result = new Table();

        Texture texture = applicationContext.textureCache.getTokenTexture(assetTokenId, "medium", true);

        Drawable drawable = new TextureRegionDrawable(texture);
        ImageButton imageButton = new ImageButton(drawable);

        imageButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                synchronized (this) {
                    arenaStaging.toggleAssetToken(assetTokenId);
                    updateBattleTables();
                }
            }
        });

        result.add(imageButton).width(imageButton.getWidth()).height(imageButton.getHeight());

        if (true) {
            result.row();
            Label label = new Label("# " + Long.toUnsignedString(assetTokenId), skin);
            label.setFontScale(1, 1);
            result.add(label);
        }

        result.row();
        Table table = GdxScene2dUtility.getTableSoldierProperties(applicationContext, soldier);

        result.add(table);

        return result;
    }

    private Table createTableDefenderCard(long assetId) {

        if (assetId == 0) {
            return null;
        }

        Soldier soldier = applicationContext.omno.state.rgame.getSoldierCloneByAssetId(assetId);

        return createTableDefenderCard(soldier);
    }

    private Table createTableDefenderCard(Soldier soldier) {

        if (soldier == null || !soldier.isValid()) {
            return null;
        }

        long assetTokenId = soldier.asset;

        Table result = new Table();

        Texture texture = applicationContext.textureCache.getTokenTexture(assetTokenId, "medium", true);

        Drawable drawable = new TextureRegionDrawable(texture);
        ImageButton imageButton = new ImageButton(drawable);

        imageButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                synchronized (this) {
                    // TODO show detail modal?
                }
            }
        });

        result.add(imageButton).width(imageButton.getWidth()).height(imageButton.getHeight());

        if (true) {
            result.row();
            Label label = new Label("# " + Long.toUnsignedString(assetTokenId), skin);
            label.setFontScale(1, 1);
            result.add(label);
        }

        result.row();
        Table table = GdxScene2dUtility.getTableSoldierProperties(applicationContext, soldier);

        result.add(table);

        return result;
    }

    public void updateBattleTables() {

        updateArena();

        if (arenaStaging == null || !arenaStaging.isValid()) {
            return;
        }

        List<Soldier> listAttacker = null;
        List<Soldier> listDefender = null;

        Battle battle = arenaStaging.getBattle();

        int chance = 0;

        if (battle != null) {
            battle.calculateBattle(false);

            if (battle.isValidPostBattle() && battle.isComplete()) {
                listAttacker = battle.attacker;
                listDefender = battle.defender;

                chance = (int) (100 * battle.monteCarlo(100));
                buttonExecute.setVisible(true);
            } else {
                buttonExecute.setVisible(false);
            }
        } else {
            buttonExecute.setVisible(false);
        }

        labelChance.setText(chance + "%");

        if (listDefender == null || listDefender.size() == 0) {
            listDefender = applicationContext.omno.state.rgame.listSoldierFromListAssetToken(arenaStaging.getDefenderSoldierAssets());
        }

        if (listAttacker == null || listAttacker.size() == 0) {
            listAttacker = applicationContext.omno.state.rgame.listSoldierFromListAssetToken(arenaStaging.getAttackerSoldierAssets());
        }

        updateTableArenaDefender(listDefender);
        updateTableArenaAttacker(listAttacker);
    }
}
