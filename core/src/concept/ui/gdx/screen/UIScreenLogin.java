package concept.ui.gdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import concept.ui.UserAccount;
import concept.ui.gdx.ApplicationContext;
import concept.utility.JsonFunction;
import concept.utility.NxtCryptography;

public class UIScreenLogin extends InputAdapter implements Screen {

    final ApplicationContext applicationContext;
    Skin skin;
    OrthographicCamera camera;
    Stage stage;

    final Table tableAccountLast;
    final Label labelAccountLastRS;

    public UIScreenLogin(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;

        synchronized (applicationContext) {
            camera = new OrthographicCamera();
            camera.setToOrtho(false, applicationContext.width, applicationContext.height);
            stage = new Stage(new FitViewport(applicationContext.width, applicationContext.height, camera));
            skin = applicationContext.skin;
        }

        Table root = new Table();
        root.setFillParent(true);
        root.defaults().pad(100);
        stage.addActor(root);

        Label labelTitle = new Label("Login", skin, "big");
        root.add(labelTitle).colspan(6).expandX();

        root.row();
        Table table = new Table();
        table.defaults().pad(50.0f);

        TextButton textButtonLogin = (TextButton) applicationContext.createButtonLogin();
        textButtonLogin.setVisible(false);

        TextField textFieldPassword = new TextField("", skin);

        textFieldPassword.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                String text = textFieldPassword.getText();

                if (text != null && text.length() != 0) {
                    NxtCryptography nxtCryptography = applicationContext.omno.ardorApi.nxtCryptographyFromString(text);

                    if (nxtCryptography != null) {
                        applicationContext.userAccount = new UserAccount(nxtCryptography, applicationContext.omno.ardorApi);
                        if (nxtCryptography.hasPrivateKey()) {
                            if (text.length() > 16) {
                                applicationContext.login();
                            } else {
                                labelTitle.setText(nxtCryptography.getAccountRS());
                                textButtonLogin.setVisible(true);
                            }
                        } else if (nxtCryptography.hasPublicKey()) {
                            labelTitle.setText(nxtCryptography.getAccountRS());
                            textButtonLogin.setVisible(true);

                        } else {
                            labelTitle.setText("?");
                            textButtonLogin.setVisible(false);
                        }
                    } else {
                        labelTitle.setText("?");
                        textButtonLogin.setVisible(false);
                    }
                }
            }
        });

        stage.setKeyboardFocus(textFieldPassword);

        table.add(textFieldPassword);
        table.add(textButtonLogin);
        table.add(applicationContext.createButtonQuit());

        root.add(table).expandX().padBottom(150);

        tableAccountLast = new Table();
        tableAccountLast.setVisible(false);
        tableAccountLast.defaults().pad(20);
        labelAccountLastRS = new Label("", skin, "big");

        String key = JsonFunction.getString(applicationContext.privateKeyJson, "lastKey", null);

        if (key != null && key.length() == 0x40) {
            NxtCryptography nxtCryptography = new NxtCryptography(key);
            labelAccountLastRS.setText(applicationContext.getArdorApi().getAccountRSAbbreviated(nxtCryptography.getAccountId()));
            tableAccountLast.setVisible(true);
        }

        TextButton textButton = new TextButton("LAST LOGIN", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                String key = JsonFunction.getString(applicationContext.privateKeyJson, "lastKey", null);

                if (key != null && key.length() == 0x40) {
                    NxtCryptography nxtCryptography = new NxtCryptography(key);
                    nxtCryptography.setAccountRS(JsonFunction.getString(applicationContext.privateKeyJson, "accountRS", null));
                    applicationContext.userAccount = new UserAccount(nxtCryptography, applicationContext.omno.ardorApi);

                    applicationContext.login();
                }
            }
        });

        tableAccountLast.add(textButton);

        tableAccountLast.add(labelAccountLastRS);

        root.row();
        root.add(tableAccountLast);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

        synchronized (this) {
            applicationContext.defaultScreenClear();
            stage.act(delta);
            stage.draw();
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

        synchronized (this) {
            stage.dispose();
        }
    }
}
