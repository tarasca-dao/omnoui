package concept.ui.gdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import concept.ui.UserAccount;
import concept.ui.gdx.ApplicationContext;
import concept.utility.JsonFunction;
import concept.utility.NxtCryptography;

import java.util.concurrent.TimeUnit;

public class UIScreenWaitSync extends InputAdapter implements Screen, Runnable {

    final ApplicationContext applicationContext;
    Stage stage;

    OrthographicCamera camera;
    Skin skin;

    int startHeight = 0;
    float progressValue = 0;

    final ProgressBar progressBar;
    boolean quit = false;
    private boolean stop = false;
    private boolean stopComplete = true;

    public void stop() {
        stop = true;
    }

    @Override
    public void run() {

        stopComplete = false;

        while(!stop) {
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException ignored) { }

            synchronized (this) {
                int blockCountTotal = 1;
                int blockCountSync = 0;

                synchronized (applicationContext.omno) {
                    if (applicationContext.omno.platformContext.economicCluster != null && applicationContext.omno.state.economicCluster != null) {
                        blockCountTotal = applicationContext.omno.platformContext.economicCluster.getHeight();
                        blockCountSync = applicationContext.omno.state.economicCluster.getHeight();
                    }
                }

                if (startHeight == 0) {
                    startHeight = blockCountSync;
                }

                int remaining = blockCountTotal - blockCountSync;
                int scale = blockCountTotal - startHeight;




                progressValue = (float) 100 * (scale - remaining) / scale;

                if (remaining <= 0) {
                    progressValue = 100;
                }
            }
        }

        stopComplete = true;
    }

    public boolean waitStopComplete() {

        if (stop) {

            while (!stopComplete) {
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException ignored) {}
            }
        }

        return stop;
    }

    public UIScreenWaitSync(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, applicationContext.width, applicationContext.height);
        stage = new Stage(new FitViewport(applicationContext.width, applicationContext.height, camera));
        skin = applicationContext.skin;

        Table root = new Table();
        root.setFillParent(true);
        root.defaults().padTop(150);
        stage.addActor(root);

        Label label = new Label("Synchronization", skin, "big");

        progressBar = new ProgressBar(0, 100.0f, 1, false, skin);

        root.add(label).colspan(3);

        root.row();
        progressBar.setValue(0.0f);
        progressBar.setAnimateDuration(0);
        root.add(progressBar).colspan(3).growX();
        root.row();

        root.add(applicationContext.createButtonQuit()).expandX();

        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

        if (quit) {
            return;
        }

        synchronized (this) {
            progressBar.setValue(progressValue);

            applicationContext.defaultScreenClear();
            stage.act(Gdx.graphics.getDeltaTime());
            stage.draw();

            if (progressValue >= 100) {
                quit = true;

                synchronized (applicationContext) {
                    String key = JsonFunction.getString(applicationContext.privateKeyJson, "lastKey", null);

                    if (key != null && key.length() == 0x40) {
                        NxtCryptography nxtCryptography = new NxtCryptography(key);
                        nxtCryptography.setAccountRS(JsonFunction.getString(applicationContext.privateKeyJson, "accountRS", null));
                        applicationContext.userAccount = new UserAccount(nxtCryptography, applicationContext.omno.ardorApi);

                        applicationContext.login();
                    } else {
                        applicationContext.setScreen(new UIScreenLogin(applicationContext));
                    }
                }
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        synchronized (this) {
            stage.getViewport().update(width, height, true);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        synchronized (this) {
            stop();
            waitStopComplete();
            stage.dispose();
        }
    }
}
