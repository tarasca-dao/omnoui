package concept.ui.gdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import concept.omno.object.PlatformToken;
import concept.ui.BattleLog;
import concept.ui.gdx.ApplicationContext;
import concept.ui.gdx.GdxScene2dUtility;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Battle;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Soldier;

import java.util.HashMap;
import java.util.List;

public class UIScreenBattleView extends InputAdapter implements Screen {

    private final ApplicationContext applicationContext;
    private final concept.omno.ApplicationContext omno;
    private final Stage stage;
    private final OrthographicCamera camera;
    private final Skin skin;

    private final Table tableBattleView;
    private final ScrollPane tableBattleViewScrollPane;
    private int battleId;
    final private BattleLog battleLog;

    public UIScreenBattleView(ApplicationContext applicationContext, int battleIdIndex) {

        this.applicationContext = applicationContext;
        omno = applicationContext.omno;
        battleLog = new BattleLog(omno, applicationContext.getArdorApi());

        synchronized (this.applicationContext) {
            camera = new OrthographicCamera();
            camera.setToOrtho(false, applicationContext.width, applicationContext.height);
            stage = new Stage(new FitViewport(applicationContext.width, applicationContext.height, camera));
            skin = applicationContext.skin;
        }

        synchronized (omno) {
            if (battleIdIndex <= 0 || battleIdIndex > omno.state.rgame.arena.battleCount) {
                this.battleId = omno.state.rgame.arena.battleCount;
            } else {
                this.battleId = battleIdIndex;
            }
        }

        tableBattleView = new Table();
        tableBattleView.defaults().pad(5);

        Table root = new Table();
        root.setFillParent(true);
        stage.addActor(root);

        Table table;

        table = new Table();
        table.defaults().pad(10.0f);

        table.add(applicationContext.createButtonLogout());
        table.add(applicationContext.createButtonArena());
        table.add(applicationContext.createButtonLeaderBoard());

        TextButton textButton;
        textButton = new TextButton("Previous", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                synchronized (applicationContext) {

                    if (battleId > 1) {
                        battleId--;
                    }

                    updateBattleView();
                    tableBattleViewScrollPane.scrollTo(0, 0, 0, 0);
                }
            }
        });

        table.add(textButton);

        textButton = new TextButton("Next", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                synchronized (applicationContext) {

                    synchronized (omno) {
                        if (battleId < omno.state.rgame.arena.battleCount) {
                            battleId++;
                        }
                    }

                    updateBattleView();
                    tableBattleViewScrollPane.scrollTo(0, 0, 0, 0);
                }
            }
        });

        table.add(textButton);

        root.add(table);

        root.row();
        tableBattleViewScrollPane = new ScrollPane(tableBattleView);
        root.add(tableBattleViewScrollPane);

        updateBattleView();
    }

    private void updateBattleView() {

        Battle battle;

        synchronized (omno) {
            if (battleId <= 0 || battleId > omno.state.rgame.arena.battleCount) {
                battleId = omno.state.rgame.arena.battleCount;
            }
        }

        battle = battleLog.loadBattle(battleId);

        tableBattleView.clear();

        if (battle == null) {
            return;
        }

        battle.calculateBattle(false);

        if (!battle.isValidPostBattle()) {
            return;
        }

        List<Integer> listDiceRolls = battle.listDiceRolls;

        int rollCount = listDiceRolls.size() / 2;

        if (rollCount == 0) {
            return;
        }

        int attackerPower = battle.getPowerAttacker();
        int defenderPower = battle.getPowerDefender();

        int powerDifference = attackerPower - defenderPower;

        Table table = new Table();
        table.defaults().pad(50);

        table.add(new Label(applicationContext.getArdorApi().getAccountRSAbbreviated(battle.defenderArmy.account), skin, "big"));

        table.row();

        Table tableInner = new Table();
        tableInner.defaults().padRight(30);
        Label label = new Label(Integer.toString(powerDifference), skin, "big");
        label.setFontScale(2);
        tableInner.add(label);

        tableInner.add(applicationContext.textureCache.getImageNamed("arrowup", "icon", 0));

        tableInner.add(applicationContext.createImageButtonArena(battle.arenaId, "medium"));

        tableInner.add(new Label("# ".concat(Integer.toString(battle.arenaId)), skin));

        table.add(tableInner);

        table.row();
        table.add(new Label(applicationContext.getArdorApi().getAccountRSAbbreviated(battle.attackerArmy.account), skin, "big"));

        tableBattleView.add(table);

        Texture textureArrowUpSymbol = applicationContext.textureCache.getNamedTexture("arrowup", "symbol");

        List<Soldier> attackerSoldiers = battle.attacker;
        List<Soldier> defenderSoldiers = battle.defender;

        int indexAttacker = 0;
        int indexDefender = 0;

        for (int i = 0; i < rollCount; i++) {

            Soldier attackerSoldier = battle.attacker.get(indexAttacker);
            Soldier defenderSoldier = battle.defender.get(indexDefender);

            int rollAttacker = listDiceRolls.get((i * 2));
            int rollDefender = listDiceRolls.get(1 + (i * 2));

            attackerPower = attackerSoldiers.get(indexAttacker).power + rollAttacker;
            defenderPower = defenderSoldiers.get(indexDefender).power + rollDefender;

//            tableBattleView.add(createTableBattleRoll(battle, i, indexAttacker, indexDefender));

            table = new Table();

            powerDifference = attackerPower - defenderPower;

            table.add(createTableSoldierCard(defenderSoldier, false));

            table.row();

            {
                Image imageUpSmall = new Image(textureArrowUpSymbol);
                Image imageDownSmall = new Image(textureArrowUpSymbol);

                imageDownSmall.setOrigin(imageDownSmall.getWidth() / 2, imageDownSmall.getHeight() / 2);
                imageDownSmall.rotateBy(180);

                tableInner = new Table();
                tableInner.defaults().padRight(15);

                if (powerDifference > 0) {

                    indexDefender++;

                    tableInner.add(imageUpSmall);

                } else if (powerDifference < 0) {

                    indexAttacker++;

                    tableInner.add(imageDownSmall);

                } else {

                    indexAttacker++;
                    indexDefender++;

                    tableInner.add(imageUpSmall);
                    tableInner.add(imageDownSmall);

                }

                tableInner.add(new Label(Integer.toString(powerDifference), skin, "big"));

                table.add(tableInner);
            }

            table.row();
            table.add(createTableSoldierCard(attackerSoldier, true));

            tableBattleView.add(table);

        }

        long newDefenderAccount = battle.defenderArmy.account;

        if (!battle.isDefenderWin) {
            newDefenderAccount = battle.attackerArmy.account;
        }

        tableInner = new Table();

        tableInner.defaults().pad(50);
        tableInner.add(new Label(applicationContext.getArdorApi().getAccountRSAbbreviated(newDefenderAccount), skin, "big"));

        tableInner.row();
        Table tableInner2 = new Table();
        tableInner.add(tableInner2);

        PlatformToken captured = battle.tokensCaptured;

        if (captured != null && !captured.isZero()) {

            HashMap<Long, Long> assetTokens = captured.getAssetTokenMap();

            if (assetTokens != null && assetTokens.size() != 0) {

                for (long item : assetTokens.keySet()) {

                    Soldier soldier = omno.state.rgame.getSoldierCloneByAssetId(item);

                    if (soldier == null) {
                        continue;
                    }

                    tableInner2.add(createTableSoldierCard(soldier, false));
                }
            }
        }

        tableBattleView.add(tableInner);

    }

    private Table createTableSoldierCard(Soldier soldier, boolean invert) {

        if (soldier == null || !soldier.isValid()) {
            return null;
        }

        long assetTokenId = soldier.asset;

        Table result = new Table();

        Texture texture = applicationContext.textureCache.getTokenTexture(assetTokenId, "medium", true);

        Drawable drawable = new TextureRegionDrawable(texture);
        ImageButton imageButton = new ImageButton(drawable);

        imageButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                synchronized (applicationContext) {
                }
            }
        });

        Label labelTokenId = new Label("# " + Long.toUnsignedString(assetTokenId), skin);
        labelTokenId.setFontScale(1, 1);
        Table tableSoldierProperties = GdxScene2dUtility.getTableSoldierProperties(applicationContext, soldier);

        if (!invert) {

            result.add(imageButton).width(imageButton.getWidth()).height(imageButton.getHeight());

            if (true) {
                result.row();
                result.add(labelTokenId);
            }

            result.row();
            result.add(tableSoldierProperties);

        } else {

            result.add(tableSoldierProperties);

            if (true) {
                result.row();
                result.add(labelTokenId);
            }

            result.row();
            result.add(imageButton).width(imageButton.getWidth()).height(imageButton.getHeight());
        }

        return result;
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

        synchronized (this) {
            applicationContext.defaultScreenClear();
            stage.act(Gdx.graphics.getDeltaTime());
            stage.draw();
        }
    }

    @Override
    public void resize(int width, int height) {

        synchronized (this) {
            stage.getViewport().update(width, height, true);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

        synchronized (this) {
            stage.dispose();
        }
    }
}
