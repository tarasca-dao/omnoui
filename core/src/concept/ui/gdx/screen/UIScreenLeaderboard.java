package concept.ui.gdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import concept.platform.EconomicCluster;
import concept.ui.BattleLog;
import concept.ui.Leaderboard;
import concept.ui.gdx.ApplicationContext;
import concept.ui.gdx.GdxScene2dUtility;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Arena;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Battle;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class UIScreenLeaderboard extends InputAdapter implements Screen, Runnable {

    private final ApplicationContext applicationContext;
    private final Stage stage;
    private final OrthographicCamera camera;
    private final Skin skin;

    final Table tableLeaderboard;
    final Leaderboard leaderboard;

    final BattleLog battleLog;
    final Table tableBattleLog;

    EconomicCluster economicCluster;
    private boolean stop = false;
    private boolean stopComplete = true;

    public void stop() {
        stop = true;
    }

    @Override
    public void run() {

        stopComplete = false;

        while(!stop) {
            synchronized (this) {
                EconomicCluster economicCluster = applicationContext.getEconomicClusterClone();

                if (this.economicCluster == null || ! this.economicCluster.isEqual(economicCluster)) {
                    this.economicCluster = economicCluster;

                    processBlock();
                }
            }

            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException ignored) { }
        }

        stopComplete = true;
    }

    public boolean waitStopComplete() {

        if (stop) {

            while (!stopComplete) {
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException ignored) {}
            }
        }

        return stop;
    }

    public UIScreenLeaderboard(ApplicationContext applicationContext) {

        this.applicationContext = applicationContext;
        concept.omno.ApplicationContext omno = applicationContext.omno;
        leaderboard = new Leaderboard(omno);
        battleLog = new BattleLog(omno, applicationContext.getArdorApi());

        synchronized (this.applicationContext) {
            camera = new OrthographicCamera();
            camera.setToOrtho(false, applicationContext.width, applicationContext.height);
            stage = new Stage(new FitViewport(applicationContext.width, applicationContext.height, camera));
            skin = applicationContext.skin;
        }

        tableLeaderboard = new Table();
        tableBattleLog = new Table();

        Table root = new Table();
        root.setFillParent(true);
        root.defaults().padTop(10).growX().growY();
        stage.addActor(root);

        Table tableLeftPane = new Table();
        Table tableRightPane = new Table();
        root.add(tableLeftPane);
        root.add(tableRightPane);

        Table table;
        table = new Table();
        table.defaults().pad(10.0f);

        table.add(applicationContext.createButtonLogout());
        table.add(applicationContext.createButtonBalance());
        table.add(applicationContext.createButtonExchange());
        table.add(applicationContext.createButtonArena());

        tableLeftPane.add(table);

        tableLeftPane.row();
        tableLeftPane.add(new ScrollPane(tableLeaderboard));
        tableRightPane.add(new ScrollPane(tableBattleLog));

        applicationContext.preloadTextures();
        processBlock(); // pre-load other textures

        Thread thread = new Thread(this);
        thread.start();
    }

    private Table createTableArenaList(List<Arena> arenaList) {

        if (arenaList == null || arenaList.size() == 0) {
            return null;
        }

        long accountId = arenaList.get(0).getAccountId();

        Table result = new Table();

        Table tableAccount = new Table();
        tableAccount.add(new Label(applicationContext.getArdorApi().getAccountRSAbbreviated(accountId), skin)).pad(10).align(Align.left);
        tableAccount.add(new Label(Long.toString(arenaList.size()), skin));
        result.add(tableAccount);

        result.row();
        Table tableArenas = new Table();
        tableArenas.defaults().pad(10);
        result.add(tableArenas);

        for (Arena item : arenaList) {
            Table table = createTableLeaderboardArena(item);
            tableArenas.add(table);
        }

        return result;
    }

    private Table createTableLeaderboardArena(Arena arena) {

        if (arena == null) {
            return null;
        }

        Table result = new Table();

        ImageButton imageButton = applicationContext.createImageButtonArena(arena.id, "small");

        result.add(imageButton).width(imageButton.getWidth()).height(imageButton.getHeight());

        if (true) {
            result.row();
            Label label = new Label("# " + arena.id, skin);
            label.setFontScale(1, 1);
            result.add(label);
        }

        result.row();
        Table table = new Table();

        GdxScene2dUtility.tableAddSymbolValue(applicationContext, table, "medium".concat(Integer.toString(arena.mediumId)), 0, 10);
        GdxScene2dUtility.tableAddSymbolValue(applicationContext, table, "domain".concat(Integer.toString(arena.domainId)), 0, 10);

        result.add(table);

        return result;
    }

    private boolean updateTableLeaderboard(Table table) {

        table.clear();

        leaderboard.update();

        List<List<Arena>> listListArena = leaderboard.getSortedListAccountArena();

        if (listListArena == null || listListArena.size() == 0) {
            return false;
        }

        boolean result = false;

        for (List<Arena> list : listListArena) {

            if (list.size() == 0) {
                continue;
            }

            long accountId = list.get(0).getAccountId();

            if (accountId == 0) {
                continue;
            }

            Table tableAccount = createTableArenaList(list);

            table.add(tableAccount);
            table.row();

            result = true;
        }

        return result;
    }

    private void updateTableBattleLog(Table table) {

        if (table == null) {
            return;
        }

        if (!battleLog.update()) {
            return;
        }

        table.clear();
        table.row();

        List<Battle> battleList = battleLog.getBattleList();

        if (battleList == null) {
            return;
        }

        int maxTableEntryCount = 50;

        for (int i = battleList.size() - 1; i >= 0; i--) {

            Battle item = battleList.get(i);

            Table tableBattleEvent = createTableBattleEvent(item);

            if (tableBattleEvent != null) {
                table.add(tableBattleEvent);
                table.row();

                if (--maxTableEntryCount <= 0) {
                    break;
                }
            }

        }
    }

    private Table createTableBattleEvent(Battle battle) {

        if (battle == null) {
            return null;
        }

        battle.calculateBattle(false);

        Table result = new Table();

        Table table = new Table();

        Table tableInner = new Table();
        Table tableInner2 = new Table();

        TextButton textButton = new TextButton("# " + battle.id, skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                synchronized (applicationContext) {
                    applicationContext.uiGotoBattleView(battle.id);
                }
            }
        });

        tableInner.add(textButton).pad(20);

        ImageButton imageButton = applicationContext.createImageButtonArena(battle.arenaId, "icon");

        tableInner2.add(imageButton).width(imageButton.getWidth()).height(imageButton.getHeight());
        tableInner2.row();
        Label label = new Label("# " + battle.arenaId, skin);
        tableInner2.add(label);
        tableInner.add(tableInner2);

        table.add(tableInner);

        String winner = "  ";

        if (battle.isDefenderWin) {
            winner = "* ";
        }

        table.row();

        table.add(new Label(winner + applicationContext.getArdorApi().getAccountRSAbbreviated(battle.defenderArmy.account), skin));

        if (battle.isDefenderWin) {
            winner = " ";
        } else {
            winner = "* ";
        }

        table.row();

        table.add(new Label(winner + applicationContext.getArdorApi().getAccountRSAbbreviated(battle.attackerArmy.account), skin));

        table.row();

        result.add(table).padTop(10);

        return result;
    }

    private void processBlock() {

        try {
            updateTableLeaderboard(tableLeaderboard);
            updateTableBattleLog(tableBattleLog);
        } catch (Exception e) {
            // rare configuration change requiring new textures loaded in thread not OpenGL context
            stop();
            waitStopComplete();
        }
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        synchronized (this) {
            applicationContext.defaultScreenClear();

            try {
                stage.act(Gdx.graphics.getDeltaTime());
                stage.draw();
            } catch (Exception ignored) {}
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stop();
        waitStopComplete();

        synchronized (this) {
            stage.dispose();
        }
    }
}
