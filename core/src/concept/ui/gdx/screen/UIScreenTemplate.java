package concept.ui.gdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import concept.platform.EconomicCluster;
import concept.ui.gdx.ApplicationContext;

import java.util.concurrent.TimeUnit;

public class UIScreenTemplate extends InputAdapter implements Screen, Runnable {

    private final ApplicationContext applicationContext;
    private final concept.omno.ApplicationContext omno;
    private final Stage stage;
    private final OrthographicCamera camera;
    private final Skin skin;


    EconomicCluster economicCluster;
    private boolean stop = false;
    private boolean stopComplete = true;

    public void stop() {
        stop = true;
    }

    @Override
    public void run() {

        stopComplete = false;

        while(!stop) {
            synchronized (this) {
                EconomicCluster economicCluster = applicationContext.getEconomicClusterClone();

                if (this.economicCluster == null || ! this.economicCluster.isEqual(economicCluster)) {
                    this.economicCluster = economicCluster;
                   
                    processBlock();
                }
            }

            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException ignored) { }
        }

        stopComplete = true;
    }

    public boolean waitStopComplete() {

        if (stop) {

            while (!stopComplete) {
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException ignored) {}
            }
        }

        return stop;
    }

    public UIScreenTemplate(ApplicationContext applicationContext) {

        this.applicationContext = applicationContext;
        omno = applicationContext.omno;

        synchronized (this.applicationContext) {
            camera = new OrthographicCamera();
            camera.setToOrtho(false, applicationContext.width, applicationContext.height);
            stage = new Stage(new FitViewport(applicationContext.width, applicationContext.height, camera));
            skin = applicationContext.skin;
        }

        Table root = new Table();
        root.setFillParent(true);
        stage.addActor(root);

        Table table;

        table = new Table();
        table.defaults().pad(10.0f);

        table.add(applicationContext.createButtonLogout());
        table.add(applicationContext.createButtonBalance());

        TextButton textButton = null;

        table.add(textButton);
    }

    private void processBlock() {

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

        synchronized (this) {
            applicationContext.defaultScreenClear();
            stage.act(Gdx.graphics.getDeltaTime());
            stage.draw();
        }
    }

    @Override
    public void resize(int width, int height) {

        synchronized (this) {
            stage.getViewport().update(width, height, true);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

        stop();
        waitStopComplete();

        synchronized (this) {
            stage.dispose();
        }
    }
}
