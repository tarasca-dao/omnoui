package concept.ui.gdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import concept.omno.object.PlatformToken;
import concept.omno.object.UserAccount;
import concept.platform.EconomicCluster;
import concept.ui.BalanceData;
import concept.ui.gdx.ApplicationContext;
import concept.ui.gdx.GdxScene2dUtility;
import concept.ui.gdx.TransactionNotifier;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Soldier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UIScreenBalance extends InputAdapter implements Screen, Runnable {

    final ApplicationContext applicationContext;
    Stage stage;
    OrthographicCamera camera;
    Skin skin;

    final BalanceData balanceData;

    final HashMap<Actor, List<Actor>> mapLabelTokenBalance = new HashMap<>();
    final HashMap<Actor, PlatformToken> mapPlatformTokenId = new HashMap<>();

    final TransactionNotifier transactionNotifier;
    final TextField textFieldValue;
    EconomicCluster economicCluster;

    private boolean stop = false;
    private boolean stopComplete = true;

    public void stop() {
        stop = true;
    }

    @Override
    public void run() {

        stopComplete = false;

        while(!stop) {
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException ignored) { }

            synchronized (this) {

                transactionNotifier.update();
                transactionNotifier.updateVisibility();

                EconomicCluster economicCluster = applicationContext.getEconomicClusterClone();

                if (this.economicCluster == null || ! this.economicCluster.isEqual(economicCluster)) {
                    this.economicCluster = economicCluster;
                    processBlock();
                }
            }
        }

        stopComplete = true;
    }

    public boolean waitStopComplete() {

        if (stop) {

            while (!stopComplete) {
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException ignored) {}
            }
        }

        return stop;
    }

    public UIScreenBalance(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        transactionNotifier = new TransactionNotifier(applicationContext);

        PlatformToken filter = applicationContext.omno.state.rgame.getTokenIds();
        balanceData = new BalanceData(applicationContext.getArdorApi(), applicationContext.getOmnoApi(), filter);

        synchronized (applicationContext) {
            camera = new OrthographicCamera();
            camera.setToOrtho(false, applicationContext.width, applicationContext.height);
            stage = new Stage(new FitViewport(applicationContext.width, applicationContext.height, camera));
            skin = applicationContext.skin;
        }

        applicationContext.userAccount.balanceSync(applicationContext.omno.platformContext.economicCluster, false);
        balanceData.platformMerge(applicationContext.userAccount.platformBalance, true);

        UserAccount userAccount = applicationContext.omno.state.userAccountState.getUserAccount(applicationContext.userAccount.nxtCryptography.getAccountId());

        if (userAccount != null && userAccount.balance != null) {
            balanceData.omnoMerge(userAccount.balance, true);
        }

        boolean readOnly = ! applicationContext.userAccount.nxtCryptography.hasPrivateKey();

        Table root = new Table();
        root.setFillParent(true);
        stage.addActor(root);

        Label label = new Label("BALANCE", skin, "big");

        if (applicationContext.userAccount.nxtCryptography != null && applicationContext.userAccount.nxtCryptography.getAccountRS() != null) {
            label.setText(applicationContext.userAccount.nxtCryptography.getAccountRS());
        }

        root.add(label).colspan(3);

        root.row();
        Table table;

        table = new Table();
        table.defaults().pad(10.0f);

        TextButton textButton;

        table.add(applicationContext.createButtonLogout());

        textButton = new TextButton("EXECUTE", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                balanceData.omnoApi.setEconomicCluster(new EconomicCluster(applicationContext.getArdorApi(), applicationContext.omno.platformContext.getHeight() - 722));
                balanceData.broadcast();
            }
        });

        if (readOnly) {
            textButton.setVisible(false);
        }

        table.add(textButton);

        textButton = new TextButton("WITHDRAW ALL", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                balanceData.omnoApi.setEconomicCluster(new EconomicCluster(applicationContext.getArdorApi(), applicationContext.omno.platformContext.getHeight() - 722));
                balanceData.withdrawAll();
            }
        });

        if (readOnly) {
            textButton.setVisible(false);
        }

        table.add(textButton);

        textButton = new TextButton("DEPOSIT ALL", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                balanceData.omnoApi.setEconomicCluster(new EconomicCluster(applicationContext.getArdorApi(), applicationContext.omno.platformContext.getHeight() - 722));
                balanceData.depositAll(applicationContext.omno.state.userAccountState.withdrawFeeNQT);
            }
        });

        if (readOnly) {
            textButton.setVisible(false);
        }

        table.add(textButton);

        table.add(applicationContext.createButtonExchange());
        table.add(applicationContext.createButtonArena());
        table.add(applicationContext.createButtonLeaderBoard());

        table.add(transactionNotifier.getTable());

        table.row();

        Texture texture;
        Drawable drawable;

        texture = applicationContext.textureCache.getNamedTexture("arrowuncheckedup", "small");
        Texture textureChecked = applicationContext.textureCache.getNamedTexture("arrowup", "small");

        drawable = new TextureRegionDrawable(texture);
        Drawable drawableChecked = new TextureRegionDrawable(textureChecked);
        ImageButton imageButtonDeposit = new ImageButton(drawable, drawable, drawableChecked);
        ImageButton imageButtonWithdraw = new ImageButton(drawable, drawable, drawableChecked);
        final boolean[] mutex = {false};

        imageButtonDeposit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                if (!mutex[0]) {

                    mutex[0] = true;

                    imageButtonDeposit.setChecked(true);

                    if (imageButtonWithdraw.isChecked()) {
                        imageButtonWithdraw.setChecked(false);
                    }

                    mutex[0] = false;

                    balanceData.isDeposit = true;
                    updateTextFieldValue();
                }
            }
        });

        table.add(imageButtonDeposit);

        imageButtonWithdraw.setTransform(true);
        imageButtonWithdraw.setOrigin(imageButtonWithdraw.getWidth() / 2, imageButtonWithdraw.getHeight() / 2);
        imageButtonWithdraw.rotateBy(180);

        imageButtonWithdraw.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                if (!mutex[0]) {

                    mutex[0] = true;

                    imageButtonWithdraw.setChecked(true);

                    if (imageButtonDeposit.isChecked()) {
                        imageButtonDeposit.setChecked(false);
                    }

                    mutex[0] = false;

                    balanceData.isDeposit = false;
                    updateTextFieldValue();
                }
            }
        });

        table.add(imageButtonWithdraw);

        textButton = new TextButton("RESET", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                balanceData.reset();
                updateLabelTokenBalance();
            }
        });

        table.add(textButton);

        addMultiplierButton(table, 1);
        addMultiplierButton(table, 10);
        addMultiplierButton(table, 100);
        addMultiplierButton(table, 1000);

        textFieldValue = new TextField("", skin);

        textFieldValue.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                synchronized (applicationContext) {

                    String text = textFieldValue.getText();

                    double doubleValue;

                    if (text != null && text.length() != 0) {

                        try {
                            doubleValue = Double.parseDouble(text);
                        } catch (Exception e) {
                            doubleValue = 1;
                        }

                        balanceData.setMultiplier(doubleValue);
                    }
                }
            }
        });

        stage.setKeyboardFocus(textFieldValue);

        table.add(textFieldValue).expandX().colspan(12);

        root.add(table).expandX();

        root.row();
        Table tableBalance = tableDepositBalance(applicationContext, balanceData.deposit);
        ScrollPane scrollPane = new ScrollPane(tableBalance);
        root.add(scrollPane);

        Thread thread = new Thread(this);
        thread.start();

        imageButtonDeposit.setChecked(true);
    }

    private void updateTextFieldValue() {
        synchronized (this) {
            double multiplier = balanceData.multiplier;

            if (!balanceData.isDeposit) {
                multiplier *= -1;
            }

            textFieldValue.setText(Double.toString(multiplier));
        }
    }

    private void addMultiplierButton(Table table, long multiplier) {

        TextButton textButton = new TextButton(Long.toUnsignedString(multiplier), skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                balanceData.setMultiplier(multiplier);
                updateTextFieldValue();
            }
        });

        table.add(textButton);
    }

    private Table tableDepositBalance(ApplicationContext applicationContext, final PlatformToken platformTokenDeposit) {

        if (applicationContext == null || platformTokenDeposit == null) {
            return null;
        }

        applicationContext.userAccount.balanceSync(applicationContext.getEconomicClusterClone(), false);

        Table root = new Table();
        root.row();

        tableAddTokenBalance(root, balanceData, 5, false);
        tableAddTokenBalance(root, balanceData, 5, true);

        return root;
    }

    private void updateLabelTokenBalance() {

        synchronized (this) {

            if (mapLabelTokenBalance.size() == 0) {
                return;
            }

            PlatformToken deposit = balanceData.deposit;
            PlatformToken withdraw = balanceData.withdraw;

            for (Actor actor : mapPlatformTokenId.keySet()) {

                PlatformToken platformTokenId = mapPlatformTokenId.get(actor);
                boolean isAsset = platformTokenId.isAssetTokenUnique();
                long key = platformTokenId.getUniqueKey();

                if (key == 0) {
                    continue;
                }

                PlatformToken deltaWithdraw = new PlatformToken();
                long value = withdraw.getValueByUniqueId(platformTokenId);

                if (value > 0) {
                    if (isAsset) {
                        deltaWithdraw.mergeAssetToken(key, value, true);
                    } else {
                        deltaWithdraw.mergeChainToken(key, value, true);
                    }
                }

                PlatformToken deltaDeposit = new PlatformToken();
                value = deposit.getValueByUniqueId(platformTokenId);

                if (value > 0) {
                    if (isAsset) {
                        deltaDeposit.mergeAssetToken(key, value, true);
                    } else {
                        deltaDeposit.mergeChainToken(key, value, true);
                    }
                }

                List<Actor> labelList = mapLabelTokenBalance.get(actor);

                Label labelOmnoValue = (Label) labelList.get(1);
                Image imageDelta = (Image) labelList.get(2);
                Label labelDeltaValue = (Label) labelList.get(3);
                Label labelPlatformValue = (Label) labelList.get(5);

                String deltaStringDeposit;
                String deltaStringWithdraw;

                if (isAsset) {
                    labelOmnoValue.setText(balanceData.getOmnoAssetTokenValueAsUnitString(key, deltaWithdraw));
                    labelPlatformValue.setText(balanceData.getPlatformAssetTokenValueAsUnitString(key, deltaDeposit));
                    deltaStringDeposit = balanceData.getDepositAssetTokenValueAsUnitString(key, null);
                    deltaStringWithdraw = balanceData.getWithdrawAssetTokenValueAsUnitString(key, null);
                } else {
                    labelOmnoValue.setText(balanceData.getOmnoChainTokenValueAsUnitString(key, deltaWithdraw));
                    labelPlatformValue.setText(balanceData.getPlatformChainTokenValueAsUnitString(key, deltaDeposit));
                    deltaStringDeposit = balanceData.getDepositChainTokenValueAsUnitString(key, null);
                    deltaStringWithdraw = balanceData.getWithdrawChainTokenValueAsUnitString(key, null);
                }

                boolean setVisible;

                setVisible = !labelOmnoValue.getText().equalsIgnoreCase("0");
                labelOmnoValue.setVisible(setVisible);
                labelList.get(0).setVisible(setVisible);

                String deltaString;

                if (deltaStringDeposit.equalsIgnoreCase("0")) {
                    deltaString = deltaStringWithdraw;
                    imageDelta.setRotation(180);
                } else {
                    deltaString = deltaStringDeposit;
                    imageDelta.setRotation(0);
                }

                labelDeltaValue.setText(deltaString);

                setVisible = !labelDeltaValue.getText().equalsIgnoreCase("0");
                labelDeltaValue.setVisible(setVisible);
                labelList.get(2).setVisible(setVisible);

                setVisible = !labelPlatformValue.getText().equalsIgnoreCase("0");
                labelPlatformValue.setVisible(setVisible);
                labelList.get(4).setVisible(setVisible);
            }
        }
    }

    int rowCounter = 0;

    private void tableAddTokenBalance(Table root, BalanceData balanceData, int width, boolean isAsset) {

        if (root == null || balanceData == null) {
            return;
        }

        Skin skin = applicationContext.skin;

        PlatformToken balanceOmno = balanceData.getOmno().clone();
        PlatformToken balancePlatform = balanceData.getPlatform().clone();

        HashMap<Long, Long> tokenPlatform;
        HashMap<Long, Long> tokenOmno;

        if (isAsset) {
            tokenPlatform = balancePlatform.getAssetTokenMap();
            tokenOmno = balanceOmno.getAssetTokenMap();
        } else {
            tokenPlatform = balancePlatform.getChainTokenMap();
            tokenOmno = balanceOmno.getChainTokenMap();
        }

        HashSet<Long> keySetMerged = new HashSet<>();

        if (tokenPlatform != null && tokenPlatform.size() != 0) {
            keySetMerged.addAll(tokenPlatform.keySet());
        }

        if (tokenOmno != null && tokenOmno.size() != 0) {
            keySetMerged.addAll(tokenOmno.keySet());
        }

        if (keySetMerged.size() == 0) {
            return;
        }

        for (long key : keySetMerged) {

            rowCounter++;

            Texture texture = applicationContext.textureCache.getTokenTexture(key, "medium", isAsset);

            Drawable drawable = new TextureRegionDrawable(texture);
            ImageButton imageButton = new ImageButton(drawable);

            imageButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {

                    synchronized (this) {
                        PlatformToken platformTokenId = mapPlatformTokenId.get(actor);
                        balanceData.mergeUnitUnique(platformTokenId);
                    }

                    updateLabelTokenBalance();
                }
            });

            PlatformToken platformTokenId = new PlatformToken();

            if (isAsset) {
                platformTokenId.mergeAssetToken(key, 1, true);
            } else {
                platformTokenId.mergeChainToken(key, 1, true);
            }

            mapPlatformTokenId.put(imageButton, platformTokenId);

            Table table = new Table(skin);
//            table.columnDefaults(1).width(50);

            table.add(imageButton).width(imageButton.getWidth()).height(imageButton.getHeight());

            Label label;

            table.row();
            Table table1;

            // show token ID #
            if (true) {
                label = new Label("# " + Long.toUnsignedString(key), skin);
                label.setFontScale(1, 1);
                table.add(label);
                table.row();
            }

            if (false) {
                if (isAsset) {

                    Soldier soldier = applicationContext.omno.state.rgame.getSoldierCloneByAssetId(key);
                    Table tableAssetProperties;

                    if (soldier != null) {
                        tableAssetProperties = GdxScene2dUtility.getTableSoldierProperties(applicationContext, soldier);
                        table.add(tableAssetProperties);
                    } else {
                        GdxScene2dUtility.tableAddSymbolValue(applicationContext, table, "empty", 0, 0);
                    }

                } else {
                    GdxScene2dUtility.tableAddSymbolValue(applicationContext, table, "empty", 0, 0);
                }

                table.row();
            }

            table1 =  createTokenLabelTableEntry(mapLabelTokenBalance, imageButton, "logocontract", 0, skin);
            table.add(table1).align(Align.left);
            table.row();

            table1 =  createTokenLabelTableEntry(mapLabelTokenBalance, imageButton, "arrowup", 0, skin);
            table.add(table1).align(Align.left);
            table.row();

            table1 =  createTokenLabelTableEntry(mapLabelTokenBalance, imageButton, "ct1", 0, skin);
            table.add(table1).align(Align.left);
            table.row();

            if (width != 0 && rowCounter > width) {
                rowCounter = 1;
                root.row();
            }

            root.add(table).padRight(10);
        }

        updateLabelTokenBalance();
    }

    private Table createTokenLabelTableEntry(final HashMap<Actor, List<Actor>> mapLabelTokenBalance, Actor key, String textureName, float textureRotation, Skin skin) {
        Table result = new Table();

        Texture texture = applicationContext.textureCache.getNamedTexture(textureName, "symbol");

        Drawable drawable = new TextureRegionDrawable(texture);
        Image image = new Image(drawable);

        image.setOrigin(image.getWidth() / 2, image.getHeight() / 2);

        if (textureRotation != 0) {
            image.rotateBy(textureRotation);
        }

        result.add(image).size(image.getWidth(), image.getHeight()).align(Align.left).padRight(10);

        Label label = new Label("0", skin);

        label.setFontScale(2, 2);
        result.add(label);

        List<Actor> labelList;

        if (mapLabelTokenBalance.size() != 0 && mapLabelTokenBalance.containsKey(key)) {
            labelList = mapLabelTokenBalance.get(key);
        } else {
            labelList = new ArrayList<>();
            mapLabelTokenBalance.put(key, labelList);
        }

        labelList.add(image);
        labelList.add(label);

        return result;
    }

    public void processBlock() {

        synchronized (applicationContext) {
            applicationContext.userAccount.balanceSync(applicationContext.omno.platformContext.economicCluster, false);

            synchronized (this) {
                balanceData.platformZero();
                balanceData.platformMerge(applicationContext.userAccount.platformBalance, true);

                balanceData.omnoZero();
                UserAccount userAccount = applicationContext.omno.state.userAccountState.getUserAccount(applicationContext.userAccount.nxtCryptography.getAccountId());

                if (userAccount != null && userAccount.balance != null) {
                    balanceData.omnoMerge(userAccount.balance, true);
                }
            }
        }

        updateLabelTokenBalance();
        updateTextFieldValue();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

        synchronized (this) {
            applicationContext.defaultScreenClear();
            stage.act(Gdx.graphics.getDeltaTime());
            stage.draw();
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stop();
        waitStopComplete();

        synchronized (this) {
            stage.dispose();
        }
    }
}
