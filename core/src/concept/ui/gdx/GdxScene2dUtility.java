package concept.ui.gdx;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import concept.omno.object.PlatformToken;
import org.tarasca.mythicalbeings.rgame.omno.service.object.Soldier;

import java.util.HashMap;

public class GdxScene2dUtility {

    public static void tableAddSymbolValue(ApplicationContext applicationContext, Table table, String textureName, String value, int scale, float pad) {

        Texture texture = applicationContext.textureCache.getNamedTexture(textureName, "symbol");
        Image image = new Image(texture);
        table.add(image).size(image.getWidth(), image.getHeight()).align(Align.left).padRight(pad);

        if (value != null && value.length() > 0) {
            Label label = new Label(value, applicationContext.skin);
            label.setFontScale(scale, scale);
            table.add(label).padRight(pad);
        }
    }

    public static void tableAddSymbolValue(ApplicationContext applicationContext, Table table, String textureName, long value, float pad) {

        Texture texture = applicationContext.textureCache.getNamedTexture(textureName, "symbol");
        Image image = new Image(texture);
        table.add(image).size(image.getWidth(), image.getHeight()).align(Align.left).padRight(pad);

        if (value != 0) {
            Label label = new Label(Long.toString(value), applicationContext.skin);
            label.setFontScale(2, 2);
            table.add(label).padRight(pad);
        }
    }

    public static void tableAddTokenValue(ApplicationContext applicationContext, Table table, long tokenId, long value, boolean isAsset, int scale, float pad) {

        if (value <= 0) {
            return;
        }

        Texture texture = applicationContext.textureCache.getTokenTexture(tokenId, "symbol", isAsset);
        Image image = new Image(texture);
        table.add(image).size(image.getWidth(), image.getHeight()).align(Align.left).padRight(pad);

        Label label = new Label(Long.toString(value), applicationContext.skin);
        label.setFontScale(scale, scale);
        table.add(label).padRight(pad);
    }

    public static void tableAddPlatformToken(ApplicationContext applicationContext, Table table, PlatformToken platformToken, int scale, float pad) {

        if (platformToken == null || !platformToken.isValid() || platformToken.isZero()) {
            return;
        }

        HashMap<Long, Long> hashMap;

        hashMap = platformToken.getChainTokenMap();

        if (hashMap != null && hashMap.size() > 0) {

            for (long id : hashMap.keySet()) {
                long value = hashMap.get(id) / applicationContext.getArdorApi().getChainToken((int) id).ONE_COIN;
                tableAddTokenValue(applicationContext, table, id, value, false, scale, pad);
            }
        }

        hashMap = platformToken.getAssetTokenMap();

        if (hashMap != null && hashMap.size() > 0) {

            for (long id : hashMap.keySet()) {
                long value = hashMap.get(id) / applicationContext.getArdorApi().getAsset(id).getUnit();
                tableAddTokenValue(applicationContext, table, id, value, true, scale, pad);
            }
        }
    }

    public static Table getTableSoldierProperties(ApplicationContext applicationContext, Soldier soldier) {

        if (soldier == null) {
            return null;
        }

        Table result = new Table();
        tableAddSymbolValue(applicationContext, result, "domain".concat(Integer.toString(soldier.domainId)), 0, 10);
        tableAddSymbolValue(applicationContext, result, "medium".concat(Integer.toString(soldier.mediumId)), 0, 10);
        tableAddSymbolValue(applicationContext, result, "rank".concat(Integer.toString(soldier.rank)), soldier.power, 10);

        return result;
    }
}
