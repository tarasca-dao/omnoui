package concept.ui.gdx;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ScreenUtils;
import concept.omno.OmnoApi;
import concept.omno.object.PlatformToken;
import concept.platform.ArdorApi;
import concept.platform.EconomicCluster;
import concept.ui.UserAccount;
import concept.ui.gdx.screen.*;
import concept.utility.JsonFunction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ApplicationContext {
    final public concept.omno.ApplicationContext omno;
    public OmnoApi omnoApi;
    public UserAccount userAccount;

    public int width = 1536;
    public int height = 864;

    public Game game;
    public Skin skin;
    public TextureCache textureCache;

    JSONObject contextJson;
    byte[] salt;
    public JSONObject privateKeyJson;

    ApplicationContext(Game game, JSONObject jsonObject) {
        omno = new concept.omno.ApplicationContext(jsonObject);
        omno.stateRootDirectory = Paths.get(Gdx.files.getLocalStoragePath() + "/" + omno.stateRootDirectory);
        omno.loadRemoteState();

        this.game = game;
        textureCache = new TextureCache(getArdorApi(), getContractAccountId());
        skin = new Skin(Gdx.files.internal("skin/glassyui/glassy-ui.json"));
//        skin = new Skin(Gdx.files.internal("skin/default/uiskin.json"));

        try {
            FileHandle fileHandle = Gdx.files.local("local/privateKey.json");
            JSONParser jsonParser = new JSONParser();
            privateKeyJson = (JSONObject) jsonParser.parse(fileHandle.readString());
        } catch (Exception e) {
            privateKeyJson = new JSONObject();
            JsonFunction.put(privateKeyJson, "privateKey", new JSONArray());
        }

        salt = new byte[0x20];
        Date date = new Date();
        Random random = new Random(date.getTime());
        random.nextBytes(salt);

        try {
            FileHandle fileHandle = Gdx.files.local("local/context.json");
            JSONParser jsonParser = new JSONParser();
            contextJson = (JSONObject) jsonParser.parse(fileHandle.readString());
            salt = JsonFunction.getBytesFromHexString(contextJson, "salt", salt);
        } catch (Exception e) {
            contextJson = new JSONObject();
        }

        JsonFunction.put(contextJson, "salt", JsonFunction.hexStringFromBytes(salt));
    }

    public void preloadTextures() {

        PlatformToken filter = omno.state.rgame.getTokenIds();

        if (filter.getAssetTokenMap() != null) {
            List<Long> listTokens = new ArrayList<>(filter.getAssetTokenMap().keySet());

            if (listTokens.size() != 0) {
                for (long id : listTokens) {
                    textureCache.getTokenTexture(id, "icon", true);
                    textureCache.getTokenTexture(id, "small", true);
                }
            }
        }

        if (filter.getChainTokenMap() != null) {
            List<Long> listTokens = new ArrayList<>(filter.getChainTokenMap().keySet());

            if (listTokens.size() != 0) {
                for (long id : listTokens) {
                    textureCache.getTokenTexture(id, "icon", false);
                    textureCache.getTokenTexture(id, "small", false);
                }
            }
        }
    }

    public long getContractAccountId() {

        if (omno == null || !omno.isConfigured) {
            return 0;
        }

        return omno.contractAccountId;
    }

    public long getUserAccountId() {

        if (userAccount == null || userAccount.nxtCryptography == null) {
            return 0;
        }

        return userAccount.nxtCryptography.getAccountId();
    }

    public PlatformToken getOmnoUserAccountBalance() {
        return omno.state.getUserAccountBalance(getUserAccountId());
    }

    public ArdorApi getArdorApi() {

        if (omno == null) {
            return null;
        }

        return omno.ardorApi;
    }

    public OmnoApi getOmnoApi() {
        return omnoApi;
    }

    public EconomicCluster getEconomicClusterClone() {

        if (omno == null) {
            return null;
        }

        return omno.state.economicCluster.clone();
    }

    public void contextSave() {

        if (contextJson == null) {
            return;
        }

        try {
            FileHandle fileHandle = Gdx.files.local("local/context.json");
            fileHandle.writeString(contextJson.toJSONString(), false);
        } catch (Exception ignored) {}
    }

    public void privateKeyJsonSave() {

        if (privateKeyJson == null) {
            return;
        }

        try {
            FileHandle fileHandle = Gdx.files.local("local/privateKey.json");
            fileHandle.writeString(privateKeyJson.toJSONString(), false);
        } catch (Exception ignored) {}
    }

    public void setScreen(Screen screen) {

        if (screen == null) {
            return;
        }

        game.setScreen(screen);
    }

    public void addPrivateKey() {

        if (userAccount == null) {
            return;
        }

        JSONArray jsonArray = JsonFunction.getJSONArray(privateKeyJson, "privateKey", null);

        if (jsonArray == null) {
            jsonArray = new JSONArray();
            JsonFunction.put(privateKeyJson, "privateKey", jsonArray);
        }

        if (userAccount.nxtCryptography.hasPrivateKey()) {
            String string = userAccount.nxtCryptography.getPrivateKeyString();
            JsonFunction.put(privateKeyJson, "lastKey", string);
            JsonFunction.put(privateKeyJson, "accountRS", userAccount.nxtCryptography.getAccountRS());

            if (!jsonArray.contains(string)) {
                JsonFunction.add(jsonArray, string);
            }

            JsonFunction.put(privateKeyJson, "privateKey", jsonArray);
        }

    }

    public void login() {
        addPrivateKey();

        if (userAccount.nxtCryptography.hasPrivateKey()) {
            omnoApi = new OmnoApi(getArdorApi(), getContractAccountId(), omno.contractName, userAccount.nxtCryptography.getPrivateKey());
            Thread thread = new Thread(omnoApi);
            thread.start();
        } else {

            if (omnoApi != null) {
                omnoApi.stop();
                omnoApi.waitStopComplete();
            }

            omnoApi = null;
        }

        userAccount.balanceSync(getEconomicClusterClone(), true);
        uiGotoBalance();
//        uiGotoExchange();
    }

    public void logout() {

        if (omnoApi != null) {
            omnoApi.stop();
            omnoApi.waitStopComplete();
        }

        setScreen(new UIScreenLogin(this));
    }

    public void uiGotoArena(int arenaId) {

        synchronized (this) {
            setScreen(new UIScreenArena(this, arenaId));
        }
    }

    public void uiGotoLeaderboard() {

        synchronized (this) {
            setScreen(new UIScreenLeaderboard(this));
        }
    }

    public void uiGotoBattleView(int battleId) {

        synchronized (this) {
            setScreen(new UIScreenBattleView(this, battleId));
        }
    }

    public void uiGotoExchange() {

        synchronized (this) {
            setScreen(new UIScreenExchange(this));
        }
    }

    public void uiGotoBalance() {

        synchronized (this) {
            userAccount.balanceSync(getEconomicClusterClone(), false);
            setScreen(new UIScreenBalance(this));
        }
    }

    public boolean isReadOnly() {
        return !userAccount.nxtCryptography.hasPrivateKey();
    }

    public Actor createButtonQuit() {

        TextButton textButton;

        textButton = new TextButton("QUIT", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                quit();
            }
        });

        return textButton;
    }

    public Actor createButtonLogout() {

        TextButton textButton;

        textButton = new TextButton("LOGOUT", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                logout();
            }
        });

        return textButton;
    }

    public Actor createButtonLogin() {

        TextButton textButton;

        textButton = new TextButton("LOGIN", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                login();
            }
        });

        return textButton;
    }

    public Actor createButtonArena() {
        TextButton textButton;

        textButton = new TextButton("ARENA", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                uiGotoArena(-1);
            }
        });

        return textButton;
    }

    public Actor createButtonLeaderBoard() {

        TextButton textButton;

        textButton = new TextButton("LEADERBOARD", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                uiGotoLeaderboard();
            }
        });

        return textButton;
    }

    public Actor createButtonExchange() {

        TextButton textButton;

        textButton = new TextButton("EXCHANGE", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                uiGotoExchange();
            }
        });

        return textButton;
    }

    public Actor createButtonBalance() {

        TextButton textButton;

        textButton = new TextButton("BALANCE", skin, "small");

        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                uiGotoBalance();
            }
        });

        return textButton;
    }

    public ImageButton createImageButtonNamedTexture(String textureName, String sizeType, float rotation) {

        Texture texture = textureCache.getNamedTexture(textureName, sizeType);

        Drawable drawable = new TextureRegionDrawable(texture);
        ImageButton imageButton = new ImageButton(drawable);

        if (rotation != 0) {
            imageButton.setTransform(true);
            imageButton.setOrigin(imageButton.getWidth() / 2, imageButton.getHeight() / 2);
            imageButton.rotateBy(rotation);
        }

        return imageButton;
    }

    public ImageButton createImageButtonArena(int id, String sizeType) {

        ImageButton imageButton = createImageButtonNamedTexture("arena".concat(Integer.toString(id)), sizeType, 0);

        imageButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                synchronized (this) {
                    uiGotoArena(id);
                }
            }
        });

        return imageButton;
    }

    public void defaultScreenClear() {
        ScreenUtils.clear(0.13f, 0.13f, 0.13f, 0);
    }

    public void quit() {

        contextSave();
        privateKeyJsonSave();
        omno.quit();

        if (omnoApi != null) {
            omnoApi.stop();
        }

        omno.waitStopComplete();

        if (omnoApi != null) {
            omnoApi.waitStopComplete();
        }

        Gdx.app.exit();
    }

    public void dispose() {

        skin.dispose();
        textureCache.dispose();
        textureCache = null;
    }
}
