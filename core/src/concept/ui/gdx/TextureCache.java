package concept.ui.gdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import concept.platform.ArdorApi;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import static com.badlogic.gdx.graphics.g2d.Gdx2DPixmap.GDX2D_FORMAT_RGBA8888;

public class TextureCache {
    HashMap<String, Texture> texture = new HashMap<>();
    HashMap<String, Pixmap> pixmap = new HashMap<>();
    TextureAtlas textureAtlas = new TextureAtlas();
    final ArdorApi ardorApi;
    long accountId;

    TextureCache(ArdorApi ardorApi, long accountId) {
        this.ardorApi = ardorApi;
        this.accountId = accountId;
    }

    public void clearTexture() {
        if (texture != null && texture.size() != 0) {

            for (Texture item : texture.values()) {
                item.dispose();
            }

            texture.clear();
        }
    }

    private void clearAll() {

        clearTexture();

        if (pixmap != null && pixmap.size() != 0) {

            for (Pixmap item : pixmap.values()) {
                item.dispose();
            }

            pixmap.clear();
        }
    }

    public Texture getTokenTexture(long tokenId, String type, boolean isAsset) {

        if (tokenId == 0) {
            return null;
        }

        String imageIdString = Long.toUnsignedString(tokenId);

        if (!isAsset) {
            imageIdString = "ct".concat(imageIdString);
        }

        String textureName;

        if (isAsset) {
            textureName = getAssetTokenTextureName(tokenId, type);
        } else {
            textureName = getChainTokenTextureName(tokenId, type);
        }

        return getTexture(imageIdString, textureName, type);
    }

    public Texture getNamedTexture(String name, String type) {

        if (name == null) {
            return null;
        }

        return getTexture(name, name.concat(type), type);
    }

    public Texture getTexture(String imageIdString, String textureName, String type) {

        if (texture.size() != 0 && texture.containsKey(textureName)) {
            return texture.get(textureName);
        }

        Texture result;

        if (pixmap.size() != 0 && pixmap.containsKey(textureName)) {
            result = new Texture(pixmap.get(textureName));
            texture.put(textureName, result);
            return result;
        }

        result = textureLoad(textureName);

        if (result != null) {
            texture.put(textureName, result);
            return result;
        }

        Pixmap pixmapNew;
        Pixmap pixmapDefault;

        byte[] pixmapBytes = ardorApi.getTaggedDataBytesByAccountPropertyReference(textureName, accountId);

        if (pixmapBytes != null) {
            directSave(textureName, pixmapBytes);
            pixmapNew = pixmapLoad(textureName);
            return addPixmap(pixmapNew, textureName, textureName, type);
        }

        if (pixmap.size() != 0 && pixmap.containsKey(imageIdString)) {
            return addPixmap(pixmap.get(imageIdString), imageIdString, textureName, type);
        }

        pixmapDefault = pixmapLoad(imageIdString);

        if (pixmapDefault != null) {
            return addPixmap(pixmapDefault, imageIdString, textureName, type);
        }

        pixmapBytes = ardorApi.getTaggedDataBytesByAccountPropertyReference(imageIdString, accountId);

        if (pixmapBytes != null) {
            directSave(imageIdString, pixmapBytes);

            pixmapDefault = pixmapLoad(imageIdString);

            if (pixmapDefault != null) {
                return addPixmap(pixmapDefault, imageIdString, textureName, type);
            }
        }

        if (pixmap != null && pixmap.containsKey(imageIdString)) {
            pixmapDefault = pixmap.get(imageIdString);
        } else {
            pixmapDefault = pixmapLoad(imageIdString);
        }

        if (pixmapDefault == null) {
            pixmapDefault = pixmapLoad("0");

            if (pixmapDefault == null) {
                pixmapDefault = new Pixmap(32, 32, Pixmap.Format.fromGdx2DPixmapFormat(GDX2D_FORMAT_RGBA8888));
            }
        }

        return addPixmap(pixmapDefault, imageIdString, textureName, type);
    }

    public Image getImageNamed(String name, String type, float rotation) {

        if (name == null || type == null) {
            return null;
        }

        Texture texture = getNamedTexture(name, type);
        Image image = new com.badlogic.gdx.scenes.scene2d.ui.Image(texture);

        if (rotation != 0) {
            image.setOrigin(image.getWidth() / 2, image.getHeight() / 2);
            image.rotateBy(180);
        }

        return image;
    }

    private Texture addPixmap(Pixmap pixmapDefault, String pixmapName, String textureName, String type) {

        if (pixmap == null) {
            return null;
        }

        Pixmap pixmapNew = pixmapResizeFrom(pixmapDefault, type);
        Texture result = new Texture(pixmapNew);
        pixmap.put(pixmapName, pixmapNew);
        texture.put(textureName, result);

        return result;
    }

    private Pixmap pixmapResizeFrom(Pixmap source, String type) {

        Pixmap result;

        switch (type) {
            default : {
                result = pixmapResizeFrom(new Pixmap(96, 96, source.getFormat()), source);
                break;
            }

            case "symbol" : {
                result = pixmapResizeFrom(new Pixmap(32, 32, source.getFormat()), source);
                break;
            }

            case "icon" : {
                result = pixmapResizeFrom(new Pixmap(64, 64, source.getFormat()), source);
                break;
            }

            case "small" : {
                result = pixmapResizeFrom(new Pixmap(128, 128, source.getFormat()), source);
                break;
            }

            case "medium" : {
                result = pixmapResizeFrom(new Pixmap(256, 256, source.getFormat()), source);
                break;
            }

            case "big" : {
                result = pixmapResizeFrom(new Pixmap(512, 512, source.getFormat()), source);
                break;
            }
        }

        return result;
    }

    private Pixmap pixmapResizeFrom(Pixmap destination, Pixmap source) {
        destination.drawPixmap(source, 0, 0, source.getWidth(), source.getHeight(), 0, 0, destination.getWidth(), destination.getHeight());
        return destination;
    }

    private Pixmap pixmapLoad(String path) {

        if (path == null) {
            return null;
        }

        FileHandle fileHandle = Gdx.files.local(getPixmapCachePath(path));

        if (fileHandle == null || !fileHandle.exists()) {
            fileHandle = Gdx.files.internal(getPixmapIncludedPath(path));
        }

        if (fileHandle == null || ! fileHandle.exists()) {
            return null;
        }

        return new Pixmap(fileHandle);
    }

    private void directSave(String textureName, byte[] data) {

        if (textureName == null || textureName.length() == 0 || data == null || data.length == 0) {
            return;
        }

        String path = getPixmapCachePath(textureName);

        try {
            Files.createDirectories(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileHandle fileHandle = Gdx.files.local(path);

        fileHandle.writeBytes(data, false);
    }

    private void pixmapSave(String textureName, Pixmap pixmap) {

        if (textureName == null || textureName.length() == 0 || pixmap == null) {
            return;
        }

        String path = getPixmapCachePath(textureName);

        try {
            Files.createDirectories(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileHandle fileHandle = Gdx.files.local(path);

        PixmapIO.writePNG(fileHandle, pixmap);
    }

    private Texture textureLoad(String path) {

        if (path == null) {
            return null;
        }

        FileHandle fileHandle = Gdx.files.local(getPixmapCachePath(path));

        if (fileHandle == null || !fileHandle.exists()) {
            fileHandle = Gdx.files.internal(getPixmapIncludedPath(path));
        }

        if (fileHandle == null || ! fileHandle.exists()) {
            return null;
        }

        return new Texture(fileHandle);
    }

    private String getPixmapIncludedPath(String textureName) {
        String result = "texture/included";

        if (textureName != null && textureName.length() != 0) {
            result = result + "/" + textureName + ".png";
        }

        return result;
    }

    private String getPixmapCachePath(String textureName) {
        String result = "texture/cache";

        if (textureName != null && textureName.length() != 0) {
            result = result + "/" + textureName + ".png";
        }

        return result;
    }

    private String getAssetTokenTextureName(long assetId, String type) {

        String result = Long.toUnsignedString(assetId);

        if (type != null) {
            result = result.concat(type);
        }

        return result;
    }

    private String getChainTokenTextureName(long assetId, String type) {

        String result = "ct" + Long.toUnsignedString(assetId);

        if (type != null) {
            result = result.concat(type);
        }

        return result;
    }

    public void dispose() {
        textureAtlas.dispose();
        clearAll();
    }
}
