package concept.ui.gdx;

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import concept.omno.OmnoApi;
import concept.platform.Transaction;

import java.util.List;

public class TransactionNotifier {

    final ApplicationContext applicationContext;
    final OmnoApi omnoApi;
    final Table table;
    final Label labelOut;
    final Label labelIn;
    final ImageButton imageButtonOut;
    final ImageButton imageButtonIn;

    public TransactionNotifier(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        this.omnoApi = applicationContext.getOmnoApi();

        table = new Table();
        labelOut = new Label("# 0", applicationContext.skin);
        labelIn = new Label("# 0", applicationContext.skin);
        imageButtonOut = applicationContext.createImageButtonNamedTexture("arrowuncheckedup", "icon", 0);
        table.add(imageButtonOut);
        table.add(labelOut);
        imageButtonIn = applicationContext.createImageButtonNamedTexture("arrowuncheckedup", "icon", 180);
        table.add(imageButtonIn);
        table.add(labelIn);
        hide();
    }

    public Table getTable() {
        return table;
    }

    public void hide() {
        table.setVisible(false);
    }

    public void show() {
        table.setVisible(true);
    }

    public boolean updateVisibility() {

        int counterOut = omnoApi.getPendingTransactionCount();

        List<Transaction> transactionList = applicationContext.getArdorApi().getUnconfirmedTransactions(2, 0, applicationContext.getContractAccountId());

        int counterIn = 0;

        if (transactionList != null) {
            counterIn = transactionList.size();
        }

        if (counterOut != 0 || counterIn != 0) {
            show();
            return true;
        } else {
            hide();
            return false;
        }
    }

    public void update() {

        if (omnoApi == null) {
            return;
        }

        int counter = omnoApi.getPendingTransactionCount();

        if (counter != 0) {
            labelOut.setText("# " + counter);
            imageButtonOut.setVisible(true);
            labelOut.setVisible(true);
        } else {
            labelOut.setText("# 0");
            imageButtonOut.setVisible(false);
            labelOut.setVisible(false);
        }

        List<Transaction> transactionList = applicationContext.getArdorApi().getUnconfirmedTransactions(2, 0, applicationContext.getContractAccountId());

        if (transactionList != null) {
            labelIn.setText("# " + transactionList.size());
            imageButtonIn.setVisible(true);
            labelIn.setVisible(true);
        } else {
            labelIn.setText("# 0");
            imageButtonIn.setVisible(false);
            labelIn.setVisible(false);
        }
    }
}
