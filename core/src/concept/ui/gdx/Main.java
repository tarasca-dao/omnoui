package concept.ui.gdx;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import concept.ui.gdx.screen.UIScreenWaitSync;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Main extends Game {
	ApplicationContext applicationContext;

	@Override
	public void create () {

		try {
			FileHandle fileHandle = Gdx.files.internal("local/configuration.json");
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(fileHandle.readString());
			applicationContext = new ApplicationContext(this, jsonObject);
		} catch (Exception e) {
			System.out.println("exception : " + e);
			Gdx.app.exit();
		}

		if (applicationContext != null) {

			if (applicationContext.omno.isConfigured) {
				Thread applicationContextThread = new Thread(applicationContext.omno);
				applicationContextThread.start();
			}

			applicationContext.setScreen(new UIScreenWaitSync(applicationContext));
		}
	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void dispose () {
		if (applicationContext != null) {
			applicationContext.dispose();
		}
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}
}
