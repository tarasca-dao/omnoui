package concept.ui.gdx.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import concept.ui.gdx.Main;

public class DesktopLauncher {

	public static void main (String[] arg) {

		LwjglApplicationConfiguration lwjglApplicationConfiguration = new LwjglApplicationConfiguration();

		lwjglApplicationConfiguration.title = "omno";
		lwjglApplicationConfiguration.width = 1280;
		lwjglApplicationConfiguration.height = 800;
		lwjglApplicationConfiguration.fullscreen = false;
		lwjglApplicationConfiguration.foregroundFPS = 60;

		new LwjglApplication(new Main(), lwjglApplicationConfiguration);
	}
}
